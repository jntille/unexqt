# unexQt
extending the qt library with re-usable custom widgets and stylesheets

## requirements
add repository to PYTHONPATH environment variable

a python environment with:
- qtsass
- pathlib
- qtpy, pyside or pyqt

## examples
![examples](tests/gui/img/all_widgets.png)
try out custom widgets [here](tests/gui)

## explaining the repository structure

### /scripts
contains a series of command-line tools

#### /scripts/process_qrc.py
command-line script to:
- generate .png from .svg according to a color scheme
- generate .qrc
- generate .qss 
- compile the .qrc into a python module 

### unexQt/widgets
extending Qt's **QtWidgets** package 
with a collection of custom widgets

### unexQt/gui
extending Qt's **QtGui** package

### unexQt/utils
a collection of global utility functions

for example:
- color conversions (e.g. rgb tuple to QColor, hex to rgb, etc.)

### unexQt/styling
everything regarding stylesheets and resources
rewrite of Colin Duquesnoy's QDarkStyleSheet (https://github.com/ColinDuquesnoy/QDarkStyleSheet)

**unexQt/stylesheeas/color_schemes**
central folder for installed color schemes (defined inside a .json)
color schemes are used for:
- stylesheets (create a temporary _variables.scss during qss compilation) 
- generating resources 

**unexQt/stylesheets/utils**
rc.py - building qt resources (creating rc images files, .qrc file and compiled rc modules)
scss.py - compile the .scss into qss stylesheets using qtsass
stylesheets.py - function to load the stylesheets with the right resources file 











