"""
WIP
This is just a lousy script to extract the material icons from the google material icons repository
"""

import sys
import os
import pathlib
import argparse
import shutil
import subprocess

from unexQt.styling.utils import color_schemes
from unexQt.styling.utils import rc

def download_qdarkstyle_svgs_from_git():
    REPO_URL = 'https://github.com/ColinDuquesnoy/QDarkStyleSheet.git'
    TMP_DIR = pathlib.Path(__file__).parent 
    REPO_DIR = TMP_DIR / "QDarkStyleSheet"
    REPO_SVG_SRC_DIR = REPO_DIR / "qdarkstyle" / "svg"
    
    if REPO_DIR.exists():
        shutil.rmtree(str(REPO_DIR))
    subprocess.call(['git', 'clone', REPO_URL, REPO_DIR])

    SVG_DIR = TMP_DIR / "qdarkstyle_icons"
    if SVG_DIR.exists():
        shutil.rmtree(str(SVG_DIR))

    # extract svgs
    shutil.copytree(REPO_SVG_SRC_DIR, SVG_DIR)

    # cleanup
    shutil.rmtree(str(REPO_DIR))

    return SVG_DIR
    
def download_material_svgs_from_git():
    # clone git repo
    REPO_URL = 'https://github.com/google/material-design-icons.git'
    TMP_DIR = pathlib.Path(__file__).parent 
    REPO_DIR = TMP_DIR / "material-design-icons"
    REPO_SVG_SRC_DIR = REPO_DIR / "src"
    
    if REPO_DIR.exists():
        shutil.rmtree(str(REPO_DIR))
    subprocess.call(['git', 'clone', REPO_URL, REPO_DIR])

    SVG_DIR = TMP_DIR / "material_io_icons"
    if SVG_DIR.exists():
        shutil.rmtree(str(SVG_DIR))
    SVG_DIR.mkdir()
    
    # extract svgs
    for category_dir in REPO_SVG_SRC_DIR.iterdir():
        for icon_dir in category_dir.iterdir():
            for icon_theme_dir in icon_dir.iterdir():
                theme = icon_theme_dir.stem

                dest_theme_dir = pathlib.Path(SVG_DIR / theme)
                dest_theme_dir.mkdir(exist_ok=True)

                icon_files = [icon_file for icon_file in icon_theme_dir.iterdir() if (icon_file.is_file() and icon_file.name == "24px.svg")]

                for file in icon_files:
                    shutil.copy(file, dest_theme_dir)
                    old_name = str(dest_theme_dir/"24px.svg")
                    new_name = str(dest_theme_dir / ("number_" + icon_dir.stem + ".svg")) if icon_dir.stem[0].isdigit() else str(dest_theme_dir/icon_dir.stem) + ".svg"
                    os.rename(old_name, new_name)

    # cleanup
    shutil.rmtree(str(REPO_DIR))

    return SVG_DIR


def generate_icon_tokens_file(tokens_file:pathlib.Path, svg_dir:pathlib.Path, rc_prefix:str, rc_subdir:str):
    """assumes that svgs have been compiled to a rc file"""
    ICON_TOKEN_TEMPLATE = "{IDENTIFIER} = ':/{prefix}/{sub_dir}/{default_png_file}'\n"

    with open(str(tokens_file), 'w') as f:
        token_lines = [ICON_TOKEN_TEMPLATE.format(
            IDENTIFIER=svg_file.stem.upper(),
            prefix=rc_prefix,
            sub_dir=rc_subdir,
            default_png_file=svg_file.stem + ".png",
            newline=os.sep
        ) for svg_file in svg_dir.glob("*.svg")]

        f.writelines(token_lines)

def main(arguments):
    # variables
    current_dir = pathlib.Path(__file__).parent
    svg_dir = current_dir / "svg"
    img_dir = current_dir / "img"
    qrc_file = current_dir / "icons.qrc"
    icon_tokens_file = current_dir / "icon_tokens.py"
    prefix = "scriptcommander"
    color_scheme_file = color_schemes.COLOR_SCHEMES_DIR / "dark.json"

    # remove old state
    shutil.rmtree(str(img_dir))
    img_dir.mkdir

    # convert svg's to pngs according to the color scheme
    with open(str(color_scheme_file), "r") as read_file:
        color_scheme_data = json.load(read_file)
    rc.create_images(svg_dir, img_dir, color_scheme_data)

    # create icons qrc file
    content = rc.generate_qrc_string_for_icons(img_dir, prefix)
    rc.write_qrc_file(content, qrc_file)

    # temp parse
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--compile_for',
                        default='pyside2',
                        choices=['pyqt', 'pyqt5', 'pyside', 'pyside2', 'qtpy', 'pyqtgraph', 'qt', 'qt5', 'all'],
                        type=str,
                        help="Choose which one would be generated.")
    args = parser.parse_args(arguments)

    # compile qrc into importable resource
    rc.create_rcc(current_dir, qrc_file, args.compile_for)

    # generate icon tokens
    generate_icon_tokens_file(icon_tokens_file, svg_dir, prefix, "img")

if __name__ == "__main__":
    #download_material_svgs_from_git()
    download_qdarkstyle_svgs_from_git()
    # generate_icon_tokens_file()

    # sys.exit(main(sys.argv[1:]))










