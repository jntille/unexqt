# -*- coding: utf-8 -*-
"""Script to process QRC files (convert .qrc to _rc.py and .rcc).

The script will attempt to compile the qrc file using the following tools:

    - pyrcc4 for PyQt4 and PyQtGraph (Python)
    - pyrcc5 for PyQt5 and QtPy (Python)
    - pyside-rcc for PySide (Python)
    - pyside2-rcc for PySide2 (Python)
    - rcc for Qt4 and Qt5 (C++)

Delete the compiled files that you don't want to use manually after
running this script.

Links to understand those tools:

    - pyrcc4: http://pyqt.sourceforge.net/Docs/PyQt4/resources.html#pyrcc4
    - pyrcc5: http://pyqt.sourceforge.net/Docs/PyQt5/resources.html#pyrcc5
    - pyside-rcc: https://www.mankier.com/1/pyside-rcc
    - pyside2-rcc: https://doc.qt.io/qtforpython/overviews/resources.html (Documentation Incomplete)
    - rcc on Qt4: http://doc.qt.io/archives/qt-4.8/rcc.html
    - rcc on Qt5: http://doc.qt.io/qt-5/rcc.html

"""

# Standard library imports
from __future__ import absolute_import, print_function

import argparse
import glob
import os
import sys
import pathlib
import shutil
import json

# Local imports
from unexQt.styling.utils import color_schemes, stylesheet, rc, scss, tokens

def main(arguments):
    """Process QRC files."""
    # Collect possible cmdline options for stylesheets and colorschemes
    discovered_stylesheets = {stylesheet_dir.stem: stylesheet_dir for stylesheet_dir in stylesheet.get_stylesheets()}
    discovered_stylesheets["all"] = None

    discovered_color_schemes = {scheme_file.stem: scheme_file for scheme_file in color_schemes.get_color_schemes()}
    discovered_color_schemes["all"] = None
    
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--stylesheet',
                        type=str,
                        choices=list(discovered_stylesheets.keys()),
                        default="all",
                        help="Discovered stylesheet"
    )
    parser.add_argument('--color_scheme',
                        type=str,
                        choices=list(discovered_color_schemes.keys()),
                        default="all",
                        help="Discovered color schemes"
    )
    parser.add_argument('--skip_rc',
                        action='store_true',
                        help="generates colored rc's from svg and builds .qrc")
    parser.add_argument('--skip_qss',
                        action='store_true',
                        help="generates .qss stylesheet for the given color scheme")
    parser.add_argument('--compile_for',
                        default='pyside2',
                        choices=['pyqt', 'pyqt5', 'pyside', 'pyside2', 'qtpy', 'pyqtgraph', 'qt', 'qt5', 'all'],
                        type=str,
                        help="Choose which one would be generated.")

    # parsing arguments 
    args = parser.parse_args(arguments)

    parsed_stylesheets = {}
    if args.stylesheet == "all":
        parsed_stylesheets = discovered_stylesheets
        del parsed_stylesheets["all"]
    else:
        parsed_stylesheets[args.stylesheet] = discovered_stylesheets[args.stylesheet]

    parsed_color_schemes = {}
    if args.color_scheme == "all":
        parsed_color_schemes = discovered_color_schemes
        del discovered_color_schemes["all"]
    else:
        parsed_color_schemes[args.color_scheme] = discovered_color_schemes[args.color_scheme]

    run(parsed_stylesheets, parsed_color_schemes, args.skip_rc, args.skip_qss, args.compile_for)

def run(stylesheets, color_schemes, skip_rc:bool, skip_qss:bool, compile_for:str):
    for stylesheet_name, stylesheet_dir in stylesheets.items():
        print(f'processing {stylesheet_name}')

        svg_dir = stylesheet_dir / "svg"
        scss_file = stylesheet_dir / "scss" / "main.scss"
        prefix = stylesheet_name

        for color_scheme_name, color_scheme_file in color_schemes.items():
            print(f'\t ... using color scheme {color_scheme_name}')

            # contains all file related to a stylesheet
            scheme_rc_dir = stylesheet_dir / "rc" / color_scheme_name
            icon_tokens_dir = stylesheet_dir / "rc"
            # ... such as icons
            img_dir = scheme_rc_dir / "img"
            # ... stylesheet 
            qss_file = scheme_rc_dir / "style.qss"
            # ... json file containing the color_scheme
            color_file = scheme_rc_dir / "color_scheme.json"
            # ... and finally the .qrc file that will be compiled 
            qrc_file = scheme_rc_dir / "style.qrc"

            with open(str(color_scheme_file), "r") as read_file:
                color_scheme_data = json.load(read_file)

            if not skip_qss:
                print('Compiling SCSS/SASS files to QSS ...')
                scss.create_qss(scss_file, qss_file, prefix, color_scheme_data)

            if not skip_rc:
                print('Generate images ...')    
                svg_sub_dirs = list(filter(lambda sub_dir: sub_dir.is_dir(), svg_dir.iterdir()))
                if svg_sub_dirs:
                    for sub_dir in svg_sub_dirs:
                        # Convert imgaes
                        rc.create_images(sub_dir, (img_dir / sub_dir.name), color_scheme_data)

                        # create icon tokens
                        print(f'Generate icon tokens file')
                        tokens_file = icon_tokens_dir / "{}_icon_tokens.py".format(sub_dir.name)
                        tokens.create_icon_tokens_file(
                            tokens_file=tokens_file, 
                            root_dir=svg_dir,
                            relative_dir=sub_dir, 
                            prefix=prefix
                        )
                else:
                    rc.create_images(svg_dir, img_dir, color_scheme_data)
                    # create icon tokens
                    print(f'Generate icon tokens file')
                    tokens_file = icon_tokens_dir / "icon_tokens.py"
                    tokens.create_icon_tokens_file(
                        tokens_file=tokens_file, 
                        root_dir=svg_dir,
                        relative_dir=svg_dir, 
                        prefix=prefix
                    )

                print('Copy color_scheme.json ...')    
                shutil.copyfile(color_scheme_file, color_file)

            print('Generate .qrc ...')    
            rc.create_qrc_file(
                img_dir,
                color_file,
                qss_file,
                qrc_file, 
                prefix=prefix)
        
            print('Compiling.qrc to _rc.py and/or .rcc ...')    
            rc.create_rcc(scheme_rc_dir, qrc_file, compile_for)

if __name__ == '__main__':
    sys.exit(main(["--skip_rc"]))
