from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.USpinboxDial import URGBDials
from unexQt.widgets.UColorWheel import UColorWheelDial

####Todo
#huePoint ellipse and center crosshair have no common midpoint (offset slightly)
#fix circle / ring antialias
class UColorWheelWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(UColorWheelWidget, self).__init__(parent)
        self.vlay = QtWidgets.QVBoxLayout()
        self.setLayout(self.vlay)

        self.colorwheel = UColorWheelDial.UColorWheelDial()
        self.vlay.addWidget(self.colorwheel)

        self.rgbdials = URGBDials.URGBDials()
        self.vlay.addWidget(self.rgbdials)


