import math
import time

from PySide2 import QtCore, QtGui, QtWidgets
from unexQt.utils.color import color_convert

UCOLORWHEEL_CROSSHAIR_COLOR = (20, 20, 20, 70)
UCOLORWHEEL_HUEDOTRING_COLOR = (255, 255, 255, 255)
UCOLORWHEEL_HUEANGLELINE_COLOR = (255, 255, 255, 70)


class UColorWheelDial(QtWidgets.QWidget):
    valueChanged = QtCore.Signal(tuple)

    def __init__(self, radius=100, outerRingWidth=10, crosshairSize = .8, parent=None):
        # calling parent class constrctor
        super(UColorWheelDial, self).__init__(parent)
        self.setMouseTracking(False)

        # initialize variables
        # pixel properties of color wheel
        self.innerWheelRadius = radius  # without the extra decorations drawn of this widget
        self.outerRingWidth = outerRingWidth
        self.widgetRadius = radius + self.outerRingWidth + 1  #? why +1 for 
        self.widgetCenterPix = (self.widgetRadius, self.widgetRadius)
        self.crosshairSize = int(self.innerWheelRadius * crosshairSize)

        #match size
        self.widgetSizeHint = QtCore.QSize(self.widgetRadius*2, self.widgetRadius*2)

        # color wheel image
        self.cwImage = self.genColorWheelImage(self.widgetRadius)
        #print self.cwImage.save("wheel.png", "png", 100)

        # data storage
        self.color = QtGui.QColor()  #this is the color value that this widget represents
        self.color.setRgbF(0.0,0.0,0.0)

        #variable for ui interaction
        self.clickPos = (self.widgetRadius, self.widgetRadius) #temp store for mouse click origin
        self.dragPos = (self.widgetRadius, self.widgetRadius)
        self.dragVector = (0,0) #resulting vector from clickpos - dragpos
        self.leftButtonPressed = False 
        self.dragTresholdTimer = None 
        self.initialHuePos = (self.widgetRadius, self.widgetRadius) #pixel position of hue dot
        self.latestHuePos = (self.widgetRadius, self.widgetRadius) #initialHuePos + mousedrag vector
        #status checks

        self.update()

    def sizeHint(self): #override
        return self.widgetSizeHint
 
    def mousePressEvent(self, event):
        #capture click position for drags relative to this position
        self.clickPos = (event.x(), event.y())
        distance = euclideanDistance(self.clickPos, self.widgetCenterPix)/self.widgetRadius

        if distance <= 1.0: #click happened inside widget
            if event.button() == QtCore.Qt.MouseButton.LeftButton:
                self.leftButtonPressed = True #color drags initiated with if lmb pressed inside widget
                self.dragTresholdTimer = time.time()
            elif event.button() == QtCore.Qt.MouseButton.MiddleButton:
                self.initialHuePos = (self.widgetRadius, self. widgetRadius)
                self.latestHuePos = (self.widgetRadius, self. widgetRadius)
                self.update()

    def mouseMoveEvent(self, event): #event will only get called if any mouse button is pressed because of setMouseTracking(false);
        if self.leftButtonPressed: 
            self.dragPos = (event.x(), event.y())
            if self.dragTresholdTimer is not None:
                if time.time() - self.dragTresholdTimer > 0.11: # avoid to register accidental short clicks with timer
                    #? maybe implement different drag modes depending on hotkey (shift or none, alt) e.g. linear according to vector, sat decrease or rmb rotate angle only?

                    #calculate new hue pos after drag 
                    self.dragVector = (self.dragPos[0] - self.clickPos[0], self.dragPos[1] - self.clickPos[1]) 
                    scaleFactor = vectorLength(self.dragVector)/750.0  #non linear 
                    self.latestHuePos = (self.initialHuePos[0]+(self.dragVector[0]*scaleFactor), self.initialHuePos[1]+(self.dragVector[1]*scaleFactor))
                    
                    #fixate hue dot to color wheel boundries
                    centerVector = self.pixelToCenterBasedVector(self.latestHuePos)
                    centerVectorLength = vectorLength(centerVector)
                    if centerVectorLength > self. innerWheelRadius: #outside color wheel, scale back to max
                        self.latestHuePos = (
                            self.widgetCenterPix[0]+centerVector[0] * (self.innerWheelRadius / centerVectorLength), 
                            self.widgetCenterPix[1]+centerVector[1] * (self.innerWheelRadius / centerVectorLength))
                    #print centerVector, centerVectorLength, self.latestHuePos
                    self.update()
  

    def mouseReleaseEvent(self, event):
        self.dragTresholdTimer = None
        if event.button() == QtCore.Qt.MouseButton.LeftButton:
            self.leftButtonPressed = False 
            self.initialHuePos = self.latestHuePos #initialHP is state before drag while lastest is the current state of the drag
            self.update()
        #reset dragpos to zero

    def paintEvent(self, event):
 
        #draw generated colorwheel
        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painterBrush = QtGui.QBrush(QtCore.Qt.SolidPattern)
        pen = QtGui.QPen()
        painter.setPen(pen)
        painter.drawImage(0, 0, self.cwImage)

        (hueFloat, satFloat) = self.getColorFromPosition(self.latestHuePos[0], self.latestHuePos[1])
                
        #draw hueline
        if self.leftButtonPressed:
            pen.setWidth(1)
            pen.setColor(color_convert.rgb_to_qcolor(UCOLORWHEEL_HUEANGLELINE_COLOR))
            pen.setStyle(QtCore.Qt.PenStyle.DashLine)
            painter.setPen(pen)
            (x1, y1, x2 ,y2) = radialLine(self.widgetCenterPix, hueFloat*360.0, self.innerWheelRadius)
            painter.drawLine(x1, y1, x2 ,y2)

        #draw huedot
        pen.setWidth(2)
        pen.setColor(color_convert.rgb_to_qcolor(UCOLORWHEEL_HUEDOTRING_COLOR))
        pen.setStyle(QtCore.Qt.PenStyle.SolidLine)
        painter.setPen(pen)
        self.color.setHsvF(hueFloat, satFloat, 1.0)
        painterBrush.setColor(self.color)
        painter.setBrush(painterBrush)
        painter.drawEllipse(QtCore.QPointF(self.latestHuePos[0], self.latestHuePos[1]),6 ,6)

        painter.end()

    def genColorWheelImage(self, widgetRadius): #generate color wheel once as bg image
        w = widgetRadius * 2 #width and height
        image = QtGui.QImage(w, w, QtGui.QImage.Format_ARGB32)
        currentColor = QtGui.QColor()  # pencolor

        for y in range(w):
            for x in range(w):
                (hueFloat, satFloat)  = self.getColorFromPosition(x,y)
                if satFloat <= 1 and satFloat*self.widgetRadius > self.innerWheelRadius:  # part of inner wheel
                    #value = 90 + (165 * max(0, min(1, (satFloat - 0.90) * 30)))  # only stupid shit to have a cool falloff
                    currentColor.setHsvF
                    currentColor.setHsvF(hueFloat, min(1,satFloat), 1, a=1.0)
                    #print("pix: {}, has hue: {}, sat: {}, val: {}".format((x, y), hueFloat, satFloat, value)) #debug
                else: #outside of radius
                    currentColor.setRgb(0, 0, 0, 0)
                image.setPixelColor(x, y, currentColor)
        
        #centerCross
        #Middle Crosshair
        painter = QtGui.QPainter(image)
        painter.setPen(color_convert.rgb_to_qcolor(UCOLORWHEEL_CROSSHAIR_COLOR))
        painter.drawLine((self.widgetCenterPix[0])+ self.crosshairSize,(self.widgetCenterPix[0]),(self.widgetCenterPix[0])- self.crosshairSize,(self.widgetCenterPix[0]))
        painter.drawLine((self.widgetCenterPix[0]),(self.widgetCenterPix[0])+ self.crosshairSize,(self.widgetCenterPix[0]),(self.widgetCenterPix[0])- self.crosshairSize)
        return image

    ##helper functions
    def pixelToCenterBasedVector(self, vec1): #returns a vector from center to pixel
        return (
        vec1[0] - self.widgetCenterPix[0] , 
        vec1[1] - self.widgetCenterPix[1] )


    def getColorFromPosition(self, x, y): #always relative to center pixel
        # transform into vector from center to pix
        vector_Center2Position = (
            x - self.widgetCenterPix[0],
            y - self.widgetCenterPix[1]
        ) 
        distanceFromCenter = euclideanDistance((x, y), self.widgetCenterPix)
        
        hueFloat = hueRotation(vector_Center2Position[0],vector_Center2Position[1])/360
        satFloat = distanceFromCenter / self.innerWheelRadius
        return (hueFloat, satFloat)
                   
    def getColor(self):
        pass #get current QColor represented by widget (for external access)

    def getColorPos(self):
        pass
    
    def setColor(self):
        pass #set current QColor represented by widget (for external access)

    def setColorPos(self):
        pass

    def setColorBrightness(self):
        pass
 

#FUNC
def vectorLength(vector):
    return math.sqrt(vector[0]**2 + vector[1]**2)

def euclideanDistance(vec1, vec2):
    # distance between two tuples
    return math.sqrt((vec2[0] - vec1[0]) ** 2 + (vec2[1] - vec1[1]) ** 2)

def hueRotation(x, y):
    return (math.degrees(math.atan2(y, x) + math.pi / 2) % 360)  # maps to 0-360 degree angles clockwise with downwards y axis
    
def radialLine(center, angle, radius):
    angle_rad = math.radians(angle)
    x1 = center[0]
    y1 = center[1]
    x2 = round(x1 + math.sin(angle_rad)*radius)
    y2 = round(y1 - math.cos(angle_rad)*radius)
    return (x1, y1, x2 ,y2)
