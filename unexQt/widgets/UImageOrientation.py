from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UImageOrientation(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super(UImageOrientation, self).__init__(parent)
        main_layout = QtWidgets.QHBoxLayout()
        main_layout.setContentsMargins(0,0,0,0)
        
        self.setLayout(main_layout)
        self._buttons = []

        self.rotate_none = QtWidgets.QToolButton()
        self.rotate_none.setIcon(icons.get_icon(material_icon_tokens.IMAGE))
        self.rotate_none.setCheckable(True)
        self._buttons.append(self.rotate_none)

        self.rotate_counterclockwise = QtWidgets.QToolButton()
        self.rotate_counterclockwise.setIcon(icons.get_icon(material_icon_tokens.ROTATE_LEFT))
        self.rotate_counterclockwise.setCheckable(True)
        self._buttons.append(self.rotate_counterclockwise)

        self.rotate_clockwise = QtWidgets.QToolButton()
        self.rotate_clockwise.setIcon(icons.get_icon(material_icon_tokens.ROTATE_RIGHT))
        self.rotate_clockwise.setCheckable(True)
        self._buttons.append(self.rotate_clockwise)

        self.rotate_upsidedown = QtWidgets.QToolButton()
        self.rotate_upsidedown.setIcon(icons.get_icon(material_icon_tokens.FLIP_CAMERA_ANDROID))
        
        self._buttons.append(self.rotate_upsidedown)

        main_layout.addWidget(self.rotate_none)
        main_layout.addWidget(self.rotate_counterclockwise)
        main_layout.addWidget(self.rotate_clockwise)
        main_layout.addWidget(self.rotate_upsidedown)

        # signals
        self.rotate_none.setCheckable(True)
        self.rotate_counterclockwise.setCheckable(True)
        self.rotate_clockwise.setCheckable(True)
        self.rotate_upsidedown.setCheckable(True)

        self.rotate_none.toggled.connect(lambda checked: self.on_toggle(self.rotate_none, checked))
        self.rotate_counterclockwise.toggled.connect(lambda checked: self.on_toggle(self.rotate_counterclockwise, checked))
        self.rotate_clockwise.toggled.connect(lambda checked: self.on_toggle(self.rotate_clockwise, checked))
        self.rotate_upsidedown.toggled.connect(lambda checked: self.on_toggle(self.rotate_upsidedown, checked))

    def on_toggle(self, button, checked):
        uncheck_buttons = self._buttons.copy()
        uncheck_buttons.remove(button)
        print(uncheck_buttons)
        
        for btn in uncheck_buttons:
            btn.setChecked(False)
        




        