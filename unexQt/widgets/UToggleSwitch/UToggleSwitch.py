# taken from https://stackoverflow.com/questions/14780517/toggle-switch-in-qt
from PySide2 import QtWidgets, QtCore, QtGui

#! TODO
## make sizing variables stylesheet properties
## retrieve colors from stylesheet 

class UToggleSwitch(QtWidgets.QAbstractButton):
    def __init__(self, parent=None, track_radius=4, knob_radius=8):
        super(UToggleSwitch, self).__init__(parent=parent)
        self._track_radius = track_radius
        self._knob_radius = knob_radius

        self._start_offset = self._knob_radius
        self._end_offset = self.width() - self._start_offset,
        self._offset = self._start_offset
        self._margin = max(0, self._knob_radius - self._track_radius)


        # colors
        palette = self.palette()
        self._track_color = {
            True: palette.highlight(),
            False: palette.dark(),
        }
        self._knob_color = {
            True: palette.highlight(),
            False: palette.light(),
        }
        self._track_opacity = 0.5

        self.setCheckable(True)
        self.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)

    def sizeHint(self):  # pylint: disable=invalid-name
        return QtCore.QSize(
            6 * self._track_radius + 2 * self._margin,
            2 * self._track_radius + 2 * self._margin,
        )

    def setChecked(self, checked):
        super().setChecked(checked)

    def resizeEvent(self, event):
        super().resizeEvent(event)
        self._end_offset = self.width() - self._start_offset

    def paintEvent(self, event):  # pylint: disable=invalid-name, unused-argument
        p = QtGui.QPainter(self)
        p.setRenderHint(QtGui.QPainter.Antialiasing, True)
        p.setPen(QtCore.Qt.NoPen)
        knob_opacity = 1.0
        if self.isEnabled():
            track_opacity = self._track_opacity
            track_brush = self._track_color[self.isChecked()]
            knob_brush = self._knob_color[self.isChecked()]
        else:
            track_opacity *= 0.8
            track_brush = self.palette().shadow()
            knob_brush = self.palette().mid()

        p.setBrush(track_brush)
        p.setOpacity(track_opacity)
        p.drawRoundedRect(
            self._margin,
            self._margin,
            self.width() - 2 * self._margin,
            self.height() - 2 * self._margin,
            self._track_radius,
            self._track_radius,
        )
        p.setBrush(knob_brush)
        p.setOpacity(knob_opacity)
        p.drawEllipse(
            self._offset - self._knob_radius,
            self._start_offset - self._knob_radius,
            2 * self._knob_radius,
            2 * self._knob_radius,
        )

    def mouseReleaseEvent(self, event):  # pylint: disable=invalid-name
        super().mouseReleaseEvent(event)
        if event.button() == QtCore.Qt.LeftButton:
            self.offset_anim = QtCore.QPropertyAnimation(self, b'offset')
            self.offset_anim.setDuration(120)
            self.offset_anim.setStartValue(self._end_offset)
            self.offset_anim.setEndValue(self._start_offset)
            if not self.isChecked():
                self.offset_anim.setDirection(QtCore.QPropertyAnimation.Forward)
            else:
                self.offset_anim.setDirection(QtCore.QPropertyAnimation.Backward)

            self.offset_anim.start(QtCore.QAbstractAnimation.DeleteWhenStopped)

    def enterEvent(self, event):  # pylint: disable=invalid-name
        self.setCursor(QtCore.Qt.PointingHandCursor)
        super().enterEvent(event)

    # stylesheet properties
    def get_offset(self):
        return self._offset
    
    def set_offset(self, offset:int):
        self._offset = offset
        self.update()

    offset = QtCore.Property(int, get_offset, set_offset)

    # def get_onColor(self):
    #     return self._onColor 

    # def set_onColor(self, onColor:QtGui.QColor):
    #     self._onColor = onColor
    
    # def get_offColor(self):
    #     return self._offColor

    # def set_offColor(self, offColor:QtGui.QColor):
    #     self._offColor = offColor

    #onColor = QtCore.Property(QtGui.QColor, get_onColor, set_onColor)
    #offColor = QtCore.Property(QtGui.QColor, get_offColor, set_offColor)