from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UMainWindow import UMainWindow

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UInfoButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(UInfoButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.INFO))

class UWarnButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(UWarnButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.WARNING))

class USaveButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(USaveButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.SAVE))

class UAddButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(UAddButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.ADD))

class UAppsButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(UAppsButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.APPS))

class UCancelButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(UCancelButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.CANCEL))

class UDeleteButton(QtWidgets.QToolButton):
    def __init__(self, parent=None):
        super(UDeleteButton, self).__init__(parent)
        self.setIcon(icons.get_icon(material_icon_tokens.CANCEL))

    