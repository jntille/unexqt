from PySide2 import QtWidgets, QtCore, QtGui

class USelectPulldown(QtWidgets.QWidget):
    """A button that acts like a dropdown and supports multiselect."""

    def __init__(self, labelText=None, emptyText='[None]', options=None, multiselect=True, parent=None):
        super(USelectPulldown, self).__init__(parent=parent)
        self.multiselect = multiselect
        self.emptyText = emptyText
        self.mainLayout = QtWidgets.QGridLayout()
        self.setLayout(self.mainLayout)
        self.label = QtWidgets.QLabel(labelText)
        self.toolButton = QtWidgets.QToolButton(parent=self)
        self.optionsMenu = QtWidgets.QMenu(self)
        self.setOptions(options)
        if self.multiselect:
            self.toolButton.setText(self.emptyText)
        else:
            self.setChecked(options[0])
        self.setupUi()
        self.setupConnections()

    def setupUi(self):
        self.mainLayout.setVerticalSpacing(1)
        self.mainLayout.addWidget(self.label, 0, 0, 1, 1)
        self.mainLayout.addWidget(self.toolButton, 1, 0, 1, 1)
        self.toolButton.setMenu(self.optionsMenu)
        self.toolButton.setPopupMode(QtWidgets.QToolButton.InstantPopup)

    def setupConnections(self):
        self.optionsMenu.triggered.connect(self.updateLabel)

    def setOptions(self, options):
        """Add options to the menu options.
        @type options: list<str>
        @param options: list of options to add
        """
        self.optionsMenu.clear()
        for option in options:
            optionAction = self.optionsMenu.addAction(option)
            optionAction.setCheckable(True)

    def getChecked(self):
        """Return the checked items.
        @rtype: list<str>
        @return: list of checked items
        """
        return [action.text() for action in self.optionsMenu.actions() if action.isChecked()]

    def clearChecked(self):
        """Set all items to be unchecked."""
        for action in self.optionsMenu.actions():
            action.setChecked(False)

    def setChecked(self, actionNames):
        """Set the given actionNames to be checked and update the label.
        @type actionNames: list<str>
        @param actionNames: list of action names to set to checked
        """
        nothingChecked = True
        for action in self.optionsMenu.actions():
            if action.text() in actionNames:
                action.setChecked(True)
                nothingChecked = False
            else:
                action.setChecked(False)
        if nothingChecked:
            if not self.multiselect:
                self.optionsMenu.actions()[0].setChecked(True)
        self.updateLabelText()

    def text(self):
        """Return the tool button's current text value.
        @rtype: str
        @return: string of the current text
        """
        return self.toolButton.text()

    def updateLabel(self, action):
        """Multiselect friendly wrapper for updating the tool button label."""
        if not self.multiselect:
            self.clearChecked()
            action.setChecked(True)
        self.updateLabelText()

    def updateLabelText(self):
        """Update the tool button's label text to be the current checked items."""
        checked = self.getChecked()
        if checked:
            actions = ", ".join(checked)
        else:
            actions = self.emptyText
        self.toolButton.setText(actions)

