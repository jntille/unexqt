from abc import ABC, abstractmethod

from PySide2 import QtWidgets, QtGui, QtCore

class UAbstractDialogButton(QtWidgets.QToolButton):
    """base class for buttons that open a dialog
    """
    def __init__(self):
        super(UAbstractDialogButton, self).__init__()

        self.clicked.connect(self.open_dialog)

    @abstractmethod
    def open_dialog(self):
        if self._dialog.exec_():
            pass
        else:
            pass

    def get_dialog(self):
        return self._dialog

    def set_dialog(self, dialog):
        self._dialog = dialog
