import pathlib

from PySide2 import QtWidgets, QtGui, QtCore

from unexQt.widgets.UButton import UAbstractDialogButton
from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UFileDialogButton(UAbstractDialogButton.UAbstractDialogButton):
    filesSelected = QtCore.Signal(list)

    def __init__(self):
        super(UFileDialogButton, self).__init__()
        self.set_dialog()
        self.setText('...')
        self.setIcon(icons.get_icon(material_icon_tokens.FOLDER_OPEN))

    def set_dialog(
            self,
            file_mode: QtWidgets.QFileDialog.FileMode = QtWidgets.QFileDialog.FileMode.ExistingFile, 
            accept_mode: QtWidgets.QFileDialog.AcceptMode = QtWidgets.QFileDialog.AcceptOpen, 
            mime_type_filters: list = [], 
            name_filters: list = []):
        dialog = QtWidgets.QFileDialog(self)
        dialog.setFileMode(file_mode)
        dialog.setAcceptMode(accept_mode)
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        if len(mime_type_filters) != 0:
            dialog.setMimeTypeFilters(mime_type_filters)
        elif len(name_filters) != 0:
            dialog.setNameFilters(name_filters)
        
        super(UFileDialogButton, self).set_dialog(dialog)
        print(self.get_dialog())

    def open_dialog(self):
        if self._dialog.exec_():
            paths = [pathlib.Path(file) for file in self._dialog.selectedFiles()]
            self.filesSelected.emit(paths)
