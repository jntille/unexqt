import os
import pathlib

from PySide2 import QtWidgets, QtGui, QtCore

from unexQt.widgets.ULineEdit import ULineEdit
from unexQt.widgets.UButton import UFileDialogButton

class UFileBrowserLine(QtWidgets.QWidget):
    """A basic compound widget to represent a line edit a directory"""

    def __init__(self, isReadOnly: bool = True):
        """TODO: to be defined. """
        super(UFileBrowserLine, self).__init__()
        self.current_file_paths = []
        self.hlay = QtWidgets.QHBoxLayout()
        self.hlay.setContentsMargins(0,0,0,0)
        self.setLayout(self.hlay)

        self.line_edit = ULineEdit.ULineEdit()
        self.line_edit.setPlaceholderText("browse")
        self.hlay.addWidget(self.line_edit)

        self.file_dialog_btn = UFileDialogButton.UFileDialogButton()
        self.hlay.addWidget(self.file_dialog_btn)

        # SIGNALS 
        self.file_dialog_btn.filesSelected.connect(self.updateFileLine)
    
    def updateFileLine(self, path_list):
        self.current_file_paths = path_list
        file_mode = self.file_dialog_btn.get_dialog().fileMode()

        if file_mode == QtWidgets.QFileDialog.ExistingFiles:
            self.line_edit.setText(str(self.current_file_paths[0].parent))
        else:
            self.line_edit.setText(str(self.current_file_paths[0]))
    
    def value(self):
        return pathlib.Path(self.line_edit.text())
