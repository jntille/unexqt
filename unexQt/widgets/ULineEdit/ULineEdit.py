from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.UAction import UClearAction, constants

class ULineEdit(QtWidgets.QLineEdit):
    tabPressed = QtCore.Signal()
    
    def __init__(self,
            defaultText: str = "",
            parent=None
            ):

        super(ULineEdit, self).__init__()
        self.setClearButtonEnabled(True)
        self.setSizePolicy(QtWidgets.QSizePolicy.Policy.MinimumExpanding, QtWidgets.QSizePolicy.Policy.Fixed)

    # override When readOnly is set to QLineEdit it disables QToolButton (clear button)
    def setClearButtonEnabled(self, enable: bool = True):
        if enable == True:
            clear_action = UClearAction.UClearAction(parent=self)
            clear_action.triggered.connect(self.clear)
            self.textChanged.connect(clear_action.dynamicVisibility)
            self.addAction(clear_action, QtWidgets.QLineEdit.TrailingPosition)
            # default state
            clear_action.dynamicVisibility(self.text())
        else:
            clear_action = constants.get_action_from_widget(self, constants.CLEAR_ACTION_OBJECTNAME)
            if clear_action is not None:
                self.removeAction(clear_action)

    # https://stackoverflow.com/questions/15802176/how-to-capture-the-key-tab-event
    def event(self,event):
        if event.type() == QtCore.QEvent.KeyPress and event.key() == QtCore.Qt.Key_Tab:
            self.tabPressed.emit()
        return QtWidgets.QLineEdit.event(self,event)
