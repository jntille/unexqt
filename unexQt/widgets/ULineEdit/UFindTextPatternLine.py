from PySide2 import QtWidgets, QtCore, QtGui
import re

from unexQt.widgets.ULineEdit import ULineEdit

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UFindTextPatternLine(QtWidgets.QWidget):
    currentMatchIndexChanged = QtCore.Signal(int)
    searchChanged = QtCore.Signal()

    def __init__(self):
        super(UFindTextPatternLine, self).__init__()
        self._current_match_index = -1
        self._total_match_number = 0
        self._current_search = ("", 0, False) 
        
        self.setup_ui()
        self.setup_signals()

    def setup_ui(self):
        self.layout = QtWidgets.QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.setLayout(self.layout)

        self.search_box = ULineEdit.ULineEdit()
        self.search_box.setClearButtonEnabled(False)
        self.search_box.setPlaceholderText("find")
        self.layout.addWidget(self.search_box)

        self.regex_mode_icon = icons.get_icon(material_icon_tokens.EXPLICIT)
        self.text_mode_icon = icons.get_icon(material_icon_tokens.FONT_DOWNLOAD)
        self.pattern_mode_action = QtWidgets.QAction()
        self.pattern_mode_action.setCheckable(True)
        self.pattern_mode_action.setIcon(self.regex_mode_icon)
        self.is_regex_pattern = True
        self.search_box.addAction(self.pattern_mode_action, QtWidgets.QLineEdit.TrailingPosition)

        self.index_label = QtWidgets.QLabel()
        self.layout.addWidget(self.index_label)

        self.next_icon = icons.get_icon(material_icon_tokens.KEYBOARD_ARROW_DOWN)
        self.next_btn = QtWidgets.QToolButton()
        self.next_btn.setStyleSheet('border: none;')
        self.next_btn.setIcon(self.next_icon)
        self.layout.addWidget(self.next_btn)
        
        self.prev_icon = icons.get_icon(material_icon_tokens.KEYBOARD_ARROW_UP)
        self.prev_btn = QtWidgets.QToolButton()
        self.prev_btn.setStyleSheet('border: none;')
        self.prev_btn.setIcon(self.prev_icon)
        self.layout.addWidget(self.prev_btn)

        self.clear_icon = icons.get_icon(material_icon_tokens.CLEAR)
        self.clear_btn = QtWidgets.QToolButton()
        self.clear_btn.setStyleSheet('border: none;')
        self.clear_btn.setIcon(self.clear_icon)
        self.layout.addWidget(self.clear_btn)

    def setup_signals(self):
        self.next_btn.clicked.connect(lambda: self.move_to_next_match_index())
        self.prev_btn.clicked.connect(lambda: self.move_to_prev_match_index())

        self.search_box.textChanged.connect(lambda text: self.searchChanged.emit())
        self.pattern_mode_action.triggered.connect(self.toggle_pattern_mode)

    def move_to_next_match_index(self):
        if self._total_match_number > 0:
            next_match_index = self._current_match_index + 1
            if next_match_index >= self._total_match_number: # was already at last match, wrap around to first 
                self._current_match_index = 0
            else:
                self._current_match_index = next_match_index
            self.update_match_label()
        self.currentMatchIndexChanged.emit(self._current_match_index)

    def move_to_prev_match_index(self):
        if self._total_match_number > 0:
            prev_match_index = self._current_match_index -1
            if prev_match_index < 0:
                self._current_match_index = self._total_match_number -1
            else:
                self._current_match_index = prev_match_index
            self.update_match_label()
        self.currentMatchIndexChanged.emit(self._current_match_index)

    def update_match_label(self):
        if self._total_match_number > 0:
            print(self._current_match_index)
            print(self._total_match_number)
            self.index_label.setText('{}/{}'.format(self._current_match_index+1, self._total_match_number))
            self.index_label.setVisible(True)
        else:
            self.index_label.setVisible(False)
            self.index_label.setText("")

    def set_current_match_state(self, index, total_match_number):
        self._current_match_index = max(index, -1)
        self._total_match_number = max(total_match_number,0)
        self.update_match_label()
    
    def toggle_pattern_mode(self):
        self.is_regex_pattern = not self.is_regex_pattern 
        self.pattern_mode_action.setChecked(self.is_regex_pattern)
    
    def get_flags(self):
        # get all active flags from menu and bitwise or them into one
        return QtCore.QRegularExpression.PatternOption.CaseInsensitiveOption

    def get_pattern(self):
        # get all active flags from menu and bitwise or them into one
        return self.search_box.text()
