from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.ULineEdit import ULineEdit
from unexQt.widgets.UAction import UValidationAction, constants
   
class UValidateLine(ULineEdit.ULineEdit):
    tabPressed = QtCore.Signal()
    validated = QtCore.Signal(QtGui.QValidator.State)
    
    def __init__(self,
            validator: QtGui.QValidator,
            parent=None
            ):
        super(UValidateLine, self).__init__(parent=parent)
        self.set_validator(validator)
        self.set_validation_action(UValidationAction.UValidationAction())

        self.setReadOnly(False)
    
        self.textChanged.connect(self.revalidate)

        # INITIALIZE 
        self.revalidate() 

    # we cannot set the validator via .setValidator() because this makes it act as an input mask which prevents entering illegal characters
    def set_validator(self, validator: QtGui.QValidator):
        self._validator = validator
    
    def get_state(self):
        return self._currentState

    def revalidate(self):
        text = self.text()
        cursor_pos = self.cursorPosition()
        self._currentState = self._validator.validate(text, cursor_pos)
        
        if self._validation_action is not None:
            self._validation_action.set_state(self._currentState)
        if self._currentState == QtGui.QValidator.Invalid:
            self.setProperty('state', 'invalid')
        elif self._currentState == QtGui.QValidator.Intermediate:
            self.setProperty('state', 'default')
        elif self._currentState == QtGui.QValidator.Acceptable:
            self.setProperty('state', 'valid')

        # update the style with new property
        self.style().unpolish(self)
        self.style().polish(self)
        self.update()

        self.validated.emit(self._currentState)

    def set_validation_action(self, validation_action):
        self.clear_validation_action()
        self.validated.connect(validation_action.set_state) 
        self.addAction(validation_action, QtWidgets.QLineEdit.LeadingPosition)

    def clear_validation_action(self):
        old_validation_action = constants.get_action_from_widget(self, constants.VALIDATION_ACTION_OBJECTNAME)
        while old_validation_action is not None:
            self.removeAction(old_validation_action)
            old_validation_action = constants.get_action_from_widget(self, constants.VALIDATION_ACTION_OBJECTNAME)
        self._validation_action = None


