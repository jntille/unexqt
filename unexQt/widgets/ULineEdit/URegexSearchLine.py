from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.ULineEdit import USearchLine

class URegexSearchLine(USearchLine.USearchLine):
    regexChanged = QtCore.Signal(QtCore.QRegularExpression)

    def __init__(self, parent=None):
        super(URegexSearchLine, self).__init__(parent)

        self._regex = QtCore.QRegularExpression()
        self._regex.setPatternOptions(QtCore.QRegularExpression.CaseInsensitiveOption)

        self.textChanged.connect(self.set_regex)

    def get_regex(self):
        return self._regex

    def get_regex_pattern_from_text(self, text):
        return r"\A{pattern}\w+".format(pattern=text.replace(" ", r"\s"))

    def set_regex(self, text):
        if text != "":
            self._regex.setPattern(self.get_regex_pattern_from_text(text))
        else:
            self._regex.setPattern('')
        self.regexChanged.emit(self._regex)
        return self._regex

