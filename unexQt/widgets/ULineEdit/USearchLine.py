from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.ULineEdit import ULineEdit
from unexQt.widgets.UAction import USearchAction, constants

class USearchLine(ULineEdit.ULineEdit):
    searchExecuted = QtCore.Signal()
    upDownPressed = QtCore.Signal(QtGui.QKeyEvent)

    def __init__(self, parent=None):
        super(USearchLine, self).__init__(parent=parent)
        self.setClearButtonEnabled(False)
        self.setPlaceholderText("search")

        search_action = USearchAction.USearchAction(parent=self)
        search_action.triggered.connect(self.searchExecuted.emit)
        self.addAction(search_action, QtWidgets.QLineEdit.TrailingPosition)

        self.tabPressed.connect(self.searchExecuted.emit)

    # line edit should ignore keypresses ment for completer http://epic-alfa.kavli.tudelft.nl/share/doc/qt4/html/tools-customcompleter.html
    def keyPressEvent(self, event):
        pressedKey = event.key()
        if pressedKey in [QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter]:
            self.searchExecuted.emit()
        elif pressedKey == QtCore.Qt.Key_Up or pressedKey == QtCore.Qt.Key_Down:
            self.upDownPressed.emit(event)
        else:
            super(USearchLine, self).keyPressEvent(event)
