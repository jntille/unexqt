from PySide2 import QtWidgets, QtCore, QtGui
from unexQt.widgets.ULine import ULine

class ULabelLine(QtWidgets.QWidget):
    """A Horizontal line with a label"""
    def __init__(self, labelText="", parent=None):
        super(ULabelLine, self).__init__(parent)
        
        self.mainLayout = QtWidgets.QGridLayout()
        self.mainLayout.setContentsMargins(0,0,0,0)
        self.mainLayout.setColumnStretch(1, 1)
        self.setLayout(self.mainLayout)


        self.label = QtWidgets.QLabel(labelText)
        self.mainLayout.addWidget(self.label, 0, 0, 1, 1)

        self.line = ULine.UHLine()
        self.mainLayout.addWidget(self.line, 1, 0, 1, 2)
    
    def setText(self, text):
        self.label.setText(text)

    def setTextFormat(self, format:QtCore.Qt.TextFormat):
        self.label.setTextFormat(format)

