from PySide2 import QtCore, QtGui, QtWidgets

class UElidedLabel(QtWidgets.QFrame):
    """ word wraps its text by its width, and elides the last visible line if some text is left out
    
    taken from https://doc.qt.io/qtforpython/overviews/qtwidgets-widgets-elidedlabel-example.html

    Args:
        QtWidgets ([type]): [description]

    Returns:
        [type]: [description]
    """
    elisionChanged = QtCore.Signal(bool) # emitted if font, text content and geometry of the widget changed

    def __init__(self, text:str, parent:QtWidgets.QWidget=None):
        super(UElidedLabel, self).__init__(parent)
        # init memeber vars
        self._elided = False # cache the current elision value to avoid recomputation
        self._text = text

        # meant to fill the width of its container and grow vertically
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)

    def setText(self, text:str):
        self._text = text
        self.update() # Changing the content require a repaint of the widget

    def text(self) -> str:
        return self._text
    
    def isElided(self) -> bool:
        return self._elided
    
    def setElided(self, elided:bool):
        self._elided = elided
    
    def paintEvent(self, event):
        super(UElidedLabel, self).paintEvent(event)

        painter = QtGui.QPainter(self)
        font_metrics = painter.fontMetrics()

        didElide = False
        line_spacing = font_metrics.lineSpacing()
        y = 0

        text_layout = QtGui.QTextLayout(self.text(), painter.font())
        text_layout.beginLayout()

        while True:
            line = text_layout.createLine()
            if not line.isValid():
                break

            line.setLineWidth(self.width())
            next_line_y = y + line_spacing

            if self.height() >= next_line_y + line_spacing:
                line.draw(painter, QtCore.QPoint(0,y))
                y = next_line_y
            else:
                last_line = self.text()[line.textStart():]
                elided_last_line = font_metrics.elidedText(last_line, QtCore.Qt.ElideRight, self.width())
                painter.drawText(QtCore.QPoint(0, y + font_metrics.ascent()), elided_last_line)
                line = text_layout.createLine()
                didElide = line.isValid()
                break

        text_layout.endLayout()

        if didElide != self.isElided():
            self.setElided(didElide)
            self.elisionChanged.emit(didElide) 

if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = UElidedLabel("asdflaslkf\naksjdflkjl asdfkajsdfljld as dflkajsdf ljasdf\n asdfjkajsdfk")
    widget.show()

    app.exec_()