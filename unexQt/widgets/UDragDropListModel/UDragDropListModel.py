from abc import ABC, abstractmethod
from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UDragDropListModel import generic_roles

class UDragDropListModel(QtCore.QAbstractListModel):
    modelDataHasChanged = QtCore.Signal()

    def __init__(self, parent=None):
        super(UDragDropListModel, self).__init__(parent)
        self.model_data = []

        self.allow_drop_on_item = True

        # signals 
        self.rowsInserted.connect(lambda: self.modelDataHasChanged.emit())
        self.rowsRemoved.connect(lambda: self.modelDataHasChanged.emit())
        self.rowsMoved.connect(lambda: self.modelDataHasChanged.emit())
        self.modelReset.connect(lambda: self.modelDataHasChanged.emit())
    
    def set_model_data(self, model_data=None):
        self.beginResetModel()
        if model_data is None:
            self.model_data = []
        else:
            self.model_data = model_data
        self.endResetModel()
    
    def get_model_data(self):
        return self.model_data
    
    def clear_model_data(self):
        self.set_model_data([])
    
    @abstractmethod
    def data(self, index, role):
        raise NotImplementedError

    def itemData(self, index:QtCore.QModelIndex):
        """Returns a map with values for all predefined roles in the model for the item at the given index."""
        # retrieve data for all roles until Qt.UserRole
        roles = super(UDragDropListModel, self).itemData(index)
        
        # retrieve data for custom roles
        for role in self.get_custom_roles():
            roles[role] = self.data(index, role)
        return roles

    def rowCount(self, index):
        return len(self.model_data)

    def mimeTypes(self):
        """Returns the list of allowed MIME types"""
        return ["application/x-generic-drag-drop"]

    # Returns an object that contains serialized items of data corresponding to the list of indexes specified. 
    # The format used to describe the encoded data is obtained from the mimeTypes() function
    def mimeData(self, indexes:list):
        mimeData = QtCore.QMimeData()
        encodedData = self.encodeData(indexes)
        mimeData.setData(self.mimeTypes()[0], encodedData)
        return mimeData

    @abstractmethod
    def dropMimeData(self, data:QtCore.QMimeData, action:QtCore.Qt.DropAction, row, column, parent):
        pass

    def encodeData(self, indexes:list):
        byteArray = QtCore.QByteArray()
        stream = QtCore.QDataStream(byteArray, QtCore.QIODevice.WriteOnly)
        
        # encode widget of drag origin
        stream.writeInt64(id(self))

        # implement writing to datastream 
        for i, index in enumerate(indexes):
            # encode index
            stream.writeInt16(index.row())
            stream.writeInt16(index.column())

            # encode item
            self.serialize_item_to_stream(
                index=index,
                stream=stream
            )
        return byteArray
    
    def decode_data(self, data:QtCore.QMimeData)-> list: 
        # decoding the dropped data
        encodedData = data.data(self.mimeTypes()[0])
        stream = QtCore.QDataStream(encodedData, QtCore.QIODevice.ReadOnly)

        # decode widget origin
        is_interal_drop = True if stream.readInt64() == id(self) else False
        
        source_indexes = []
        decoded_items = []
        while not stream.atEnd():
            # decode index
            row = stream.readInt16()
            col = stream.readInt16()
            source_indexes.append((row, col))
            # decode item
            item = self.deserialize_item_from_stream(stream)
            decoded_items.append(item)

        return source_indexes, decoded_items, is_interal_drop

    @abstractmethod
    def serialize_item_to_stream(self, index, stream:QtCore.QDataStream) -> QtCore.QDataStream:
        """can be overridden by subclasses of the generic drag drop model to allow for custom item serialization"""
        raise NotImplementedError

    @abstractmethod
    def deserialize_item_from_stream(self, stream:QtCore.QDataStream):
        """can be overridden by subclasses of the generic drag drop model to allow for custom item serialization"""
        raise NotImplementedError

    def insert_rows_with_data(self, row:int, items:list):
        """inserts items many rows with data"""
        self.beginInsertRows(QtCore.QModelIndex(), row, row + len(items) - 1)
        self.model_data[row:row] = items 
        self.endInsertRows()
        return True

    def insertRows(self, row, count, parent):
        """inserts count empty rows"""
        if (count < 1 or row < 0 or row > self.rowCount(QtCore.QModelIndex())):
            return False

        self.beginInsertRows(QtCore.QModelIndex(), row, row + count - 1)
        self.model_data[row:row] = [dict() for i in range(count)]
        self.endInsertRows()
        return True

    def removeRow(self, row, parent):
        self.removeRows(row, 1, parent)

    def removeRows(self, row, count, parent):
        self.beginRemoveRows(QtCore.QModelIndex(), row, row + count - 1)
        for i in range(0, count):
            self.model_data.pop(row + i)
        self.endRemoveRows()
        return True

    def moveRows(self, sourceParent:QtCore.QModelIndex, sourceRow:int, count:int, destinationParent:QtCore.QModelIndex, destinationChild:int):
        """ movs count rows starting with the given sourceRow under parent sourceParent to row destinationChild under parent destinationParent"""
        if (sourceRow < 0 or count <= 0 or destinationChild < 0): # EXTEND
            return False

        self.beginMoveRows(sourceParent, sourceRow, sourceRow + count -1, destinationParent, destinationChild)
        fromRow = sourceRow
        if (destinationChild < sourceRow):
            fromRow = fromRow + count -1
        else:
            destinationChild = destinationChild - 1
        while (count):
            self.model_data.insert(destinationChild, self.modelData.pop(fromRow))
        self.endMoveRows()

        self.modelDataHasChanged.emit()
        return True

    def canDropMimeData(self, data, action, row, column, parent):
        if not data.hasFormat(self.mimeTypes()[0]):
            return False
        if action not in [QtCore.Qt.MoveAction, QtCore.Qt.CopyAction]:
            return False
    
        return True

    def supportedDropActions(self):
        return QtCore.Qt.MoveAction | QtCore.Qt.CopyAction        

    def flags(self, index):
        if not (index.isValid()):
            return super(UDragDropListModel, self).flags(index) | QtCore.Qt.ItemIsDropEnabled
        else: 
            base_flags = super(UDragDropListModel, self).flags(index) | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDragEnabled 
            if self.allow_drop_on_item:
                base_flags = base_flags | QtCore.Qt.ItemIsDropEnabled            
            return base_flags 

    def set_allow_drop_on_item(self, allowed:bool):
        self.allow_drop_on_item = allowed

    def get_custom_roles(self):
        return [generic_roles.ItemDataRole]