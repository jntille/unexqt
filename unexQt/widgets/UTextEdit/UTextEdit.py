from PySide2 import QtWidgets, QtCore, QtGui
import time
import re

from unexQt.widgets.UTextEdit import LineNumberArea
from unexQt.utils.color import color_convert
from unexQt.styling.utils.color_schemes import get_color_scheme_from_rc

class UTextEdit(QtWidgets.QPlainTextEdit):
    """A custom Text edit
    """
    mousePressedSignal = QtCore.Signal(object)
    
    def __init__(self, parent=None):
        """
        """
        super(UTextEdit, self).__init__(parent)

        # some settings
        self.setReadOnly(True)
        self.setMaximumBlockCount(20000)

        self.master_document = QtGui.QTextDocument() # always log against master
        self.master_doclay = QtWidgets.QPlainTextDocumentLayout(self.master_document)
        self.master_document.setDocumentLayout(self.master_doclay)
        
        self.proxy_document = QtGui.QTextDocument() # display the filtered document
        self.proxy_doclay = QtWidgets.QPlainTextDocumentLayout(self.proxy_document)
        self.proxy_document.setDocumentLayout(self.proxy_doclay)
        self.setDocument(self.proxy_document)

        self._line_num_area = LineNumberArea.LineNumberArea(self)
        self._color_scheme = get_color_scheme_from_rc(':/unexQt/color_scheme.json')
        
        self.line_number_area_bg_color = color_convert.hex_to_qcolor(self._color_scheme['COLOR_BACKGROUND_DARK'])
        self.line_number_color = color_convert.hex_to_qcolor(self._color_scheme['COLOR_FOREGROUND_NORMAL'])

        # members
        self._content_timestamp = 0
        self._search_timestamp = 0
        self._first_visible_index = 0
        self._last_visible_index = 0

        self._matches_to_highlight = set()
        self._matches_label = QtWidgets.QLabel()

        self._cursor = self.textCursor()
        pos = QtCore.QPoint(0, 0)
        self._highlight_cursor = self.cursorForPosition(pos)

        # text formatting related 
        self._format = QtGui.QTextCharFormat()
        self._format.setBackground(QtCore.Qt.red)
        self.font = self.document().defaultFont()
        self.font.setFamily('Courier New') # fixed width font
        self.document().setDefaultFont(self.font)
        self.reset_text_format = QtGui.QTextCharFormat()
        self.reset_text_format.setFont(self.document().defaultFont())

        # right click context menu
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.copy_action = QtWidgets.QAction('Copy', self)
        self.copy_action.setStatusTip('Copy Selection')
        self.copy_action.setShortcut('Ctrl+C')
        self.addAction(self.copy_action)

        # Initialize state
        self.updateLineNumberAreaWidth()
        self.highlightCurrentLine()

        # Signals
        # linearea
        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.cursorPositionChanged.connect(self.highlightCurrentLine)

        # copy
        self.customContextMenuRequested.connect(self.context_menu)
        self.copy_action.triggered.connect(lambda: self.copy_selection(QtGui.QClipboard.Mode.Clipboard))

    def appendMessage(self, msg:str, document: QtGui.QTextDocument):
        cursor = QtGui.QTextCursor(document)
        cursor.movePosition(QtGui.QTextCursor.MoveOperation.End, QtGui.QTextCursor.MoveMode.MoveAnchor)
        cursor.beginEditBlock()
        cursor.insertBlock()
        cursor.insertText(msg)
        cursor.endEditBlock()
        
        self._content_timestamp = time.time()

    def context_menu(self):
        """
        A custom context menu to pop up when the user Right-Clicks in the
        Log View. Triggered by the customContextMenuRequested signal
        """

        self._context_menu = QtWidgets.QMenu(self)
        self._context_menu.addAction(self.copy_action)
        self._context_menu.exec_(QtGui.QCursor.pos())

    def mouseReleaseEvent(self, event):
        """
        This is used to trigger the text highlighting update at the parent's
        level
        * For performance sake, we're only highlighting visible matches, which
        is why we need to know when the user scrolls manually
        @param event: The mouse release event
        type event: PyQt4.QtGui.QMouseEvent
        @postcondition: The mousePressedSignal is emitted (triggers highlights
                        for surronding matches), and the current selection is
                        stored in the "selection" (AKA. middle-mouse) clipboard
        """

        super(UTextEdit, self).mouseReleaseEvent(event)
        pos = event.pos()
        self.mousePressedSignal.emit(pos)
        self.copy_selection(QtGui.QClipboard.Selection)

    def scrollContentsBy(self, *args, **kwargs):
        """
        Overriding to make sure the line numbers area is updated when scrolling
        """

        self._line_num_area.repaint()
        return QtWidgets.QPlainTextEdit.scrollContentsBy(self, *args, **kwargs)

    def copy_selection(self, mode):
        """
        Copy (Ctrl + C) action. Stores the currently selected text in the
        clipboard.
        @param mode: The QClipboard mode value (QtGui.QClipboard.Clipboard = GLOBAL
                                                QtGui.QClipboard.Selection = Selection (middle-mouse))
        @type mode: int
        """

        selection = self.textCursor().selection()
        QtWidgets.QApplication.clipboard().setText('', mode)
        QtWidgets.QApplication.clipboard().setText(selection.toPlainText(), mode)

    def lineNumberAreaWidth(self):
        """
        calculates the width of the LineNumberArea widget. 
        take the number of lines, multiplied by the maximum width of a digit with the current font
        """

        digits = 1
        count = max(1, self.blockCount())
        while count >= 10:
            count /= 10
            digits += 1
        space = 3 + self.fontMetrics().width('9') * digits
        return space

    def updateLineNumberAreaWidth(self):
        """
        Sets the width for the line number area to the recommended width based
        on line count and font being used
        """

        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)

    def updateLineNumberArea(self, rect, dy):
        """
        Calls the necessary methods to update the size and content of the
        line number area.
        * This slot is connected to the updateRequest signal of this object
        @param rect: The rect to update
        @type rect: QtCore.QRect
        @param dy: The scroll value (will be 0 if the update request was
                   triggerred by an event other than scroll)
        @type dy: int
        """

        if dy:
            self._line_num_area.scroll(0, dy)
        else:
            self._line_num_area.update(0,
                                       rect.y(),
                                       self._line_num_area.width(),
                                       rect.height())

        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth()

    def highlightCurrentLine(self):
        """
        Highlights the line where the cursor is
        @postcondition: The line where the cursor is will have a different
                        background color
        """

        crnt_selection = QtWidgets.QTextEdit.ExtraSelection()
        line_color = QtGui.QColor(QtCore.Qt.red).lighter(12)
        crnt_selection.format.setBackground(line_color)
        crnt_selection.format.setProperty(QtGui.QTextFormat.FullWidthSelection,
                                          True)
        crnt_selection.cursor = self.textCursor()
        crnt_selection.cursor.clearSelection()
        self.setExtraSelections([crnt_selection])

    def resizeEvent(self, event):
        """
        Overriding to make sure the line-number area also gets resized when the
        text-editor widget gets resized
        @param event: The resize event
        @type event: QtGui.QResizeEvent
        """

        super(UTextEdit, self).resizeEvent(event)
        contents_rect = self.contentsRect()
        self._line_num_area.setGeometry(
            QtCore.QRect(
                contents_rect.left(),
                contents_rect.top(),
                self.lineNumberAreaWidth(),
                contents_rect.height()
            )
        )

    def lineNumberAreaPaintEvent(self, event):
        """
        lineNumberAreaPaintEvent() is called from LineNumberArea whenever it receives a paint event. 
        Paints the line numbers in the line-number area.
        """
        painter = QtGui.QPainter(self._line_num_area)
        painter.setFont(self.document().defaultFont())

        # start off by painting the widget's background.
        painter.fillRect(event.rect(), self.line_number_area_bg_color)

        # loop through all visible lines and paint the line numbers in the extra area for each line
        #  in a plain text edit each line will consist of one QTextBlock; though, if line wrapping is enabled, a line may span several rows in the text edit's viewport.

        # get the top and bottom y-coordinate of the text block
        block = self.firstVisibleBlock()
        blockNum = block.blockNumber()

        blockGeo = self.blockBoundingGeometry(block)
        top = round(blockGeo.translated(self.contentOffset()).top())
        bottom = round(top + self.blockBoundingRect(block).height())
        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(blockNum + 1)

                painter.setPen(self.line_number_color)
                painter.drawText(
                    0,
                    top,
                    self._line_num_area.width(),
                    self.fontMetrics().height(),
                    QtCore.Qt.AlignRight,
                    number
                )
            block = block.next()
            top = bottom
            bottom = top + round(self.blockBoundingRect(block).height())
            blockNum += 1
        
        painter.end()

    def _move_to_match(self, pos, length):
        """
        Moves the cursor in the content box to the given pos, then moves it
        forwards by "length" steps, selecting the characters in between
        @param pos: The starting position to move the cursor to
        @type pos: int
        @param length: The number of steps to move+select after the starting
                       index
        @type length: int
        @postcondition: The cursor is moved to pos, the characters between pos
                        and length are selected, and the content is scrolled
                        up/down to ensure the cursor is visible
        """

        self._cursor.setPosition(pos)
        self._cursor.movePosition(QtGui.QTextCursor.Right,
                                  QtGui.QTextCursor.KeepAnchor,
                                  length)
        self.setTextCursor(self._cursor)
        self.ensureCursorVisible()

    def _find_text(self, pattern:str, flags:int, isRegexPattern:bool):
        """
        Finds and stores the list of text fragments matching the search pattern
        entered in the search box.
        @postcondition: The text matching the search pattern is stored for
                        later access & processing
        """

        prev_search = self._current_search
        self._current_search = (pattern, flags, isRegexPattern)
        search_has_changed = (
            self._current_search[0] != prev_search[0] or 
            self._current_search[1] != prev_search[1] or 
            self._current_search[2] != prev_search[2]
        )

        if not self._current_search[0]:  # Nothing to search for, clear search data
            self._clear_search_data()
            return

        if self._content_timestamp <= self._search_timestamp and not search_has_changed:
            self._move_to_next_match()
            return

        # New Search
        self._clear_search_data()
        try:
            match_objects = re.finditer(str(pattern), self.toPlainText(), flags)
            for match in match_objects:
                index = match.start()
                length = len(match.group(0))
                self._matches.append((index, length))
            if not self._matches:
                self._matches_label.setStyleSheet('QLabel {color : gray}')
                self._matches_label.setText('No Matches Found')
            self._matches_to_highlight = set(self._matches)
            self._update_visible_indices()
            self._highlight_matches()
            self._search_timestamp = time.time()

            # Start navigating
            self._current_match = 0
            self._move_to_next_match()
        except re.error as err:
            self._matches_label.setText('ERROR: %s' % str(err))
            self._matches_label.setStyleSheet('QLabel {color : indianred}')

    def _highlight_matches(self):
        """
        Highlights the matches closest to the current match
        (current = the one the cursor is at)
        (closest = up to 300 matches before + up to 300 matches after)
        @postcondition: The matches closest to the current match have a new
                        background color (Red)
        """

        if not self._matches_to_highlight or not self._matches:
            return  # nothing to match

        # Update matches around the current one (300 before and 300 after)
        highlight = self._matches[max(self._current_match - 300, 0):
                                  min(self._current_match + 300, len(self._matches))]

        matches = list(set(highlight).intersection(self._matches_to_highlight))
        for match in matches:
            self._highlight_cursor.setPosition(match[0])
            self._highlight_cursor.movePosition(QtGui.QTextCursor.Right,
                                                QtGui.QTextCursor.KeepAnchor,
                                                match[-1])
            self._highlight_cursor.setCharFormat(self._format)
            self._matches_to_highlight.discard(match)

    def _clear_search_data(self):
        """
        Removes the text in the search pattern box, clears all highlights and
        stored search data
        @postcondition: The text in the search field is removed, match list is
                        cleared, and format/selection in the main content box
                        are also removed.
        """

        self._matches = []
        self._matches_to_highlight = set()
        self._search_timestamp = 0
        self._matches_label.setText('')

        format = QtGui.QTextCharFormat()
        self._highlight_cursor.setPosition(QtGui.QTextCursor.Start)
        self._highlight_cursor.movePosition(QtGui.QTextCursor.End, mode=QtGui.QTextCursor.KeepAnchor)
        self._highlight_cursor.setCharFormat(format)
        self._highlight_cursor.clearSelection()

    def _update_visible_indices(self):
        """
        Updates the stored first & last visible text content indices so we
        can focus operations like highlighting on text that is visible
        @postcondition: The _first_visible_index & _last_visible_index are
                        up to date (in sync with the current viewport)
        """

        viewport = self.viewport()
        try:
            top_left = QtCore.QPoint(0, 0)
            bottom_right = QtCore.QPoint(viewport.width() - 1,
                                         viewport.height() - 1)
            first = self.cursorForPosition(top_left).position()
            last = self.cursorForPosition(bottom_right).position()
            self._first_visible_index = first
            self._last_visible_index = last
        except IndexError:  # When there's nothing in the content box
            pass
