from PySide2 import QtWidgets, QtCore, QtGui

class LineNumberArea(QtWidgets.QWidget):
    """
    We paint the line numbers on this widget, and place it over the CodeEditor's viewport()'s left margin area.
    """

    def __init__(self, editor):
        """
        Creates the LineNumberArea instance
        @param editor: The text editor widget to attach this widget to
        @type editor: QtWidgets.QPlainTextEdit or QtWidgets.QTextEdit
        """

        super(LineNumberArea, self).__init__(editor)
        self.editor = editor
    
    def sizeHint(self):
        return QtCore.QSize(self.editor.lineNumberAreaWidth(), 0)

    def paintEvent(self, event):
        """
        The paint event for this widget will trigger the text update on the
        editor widget
        @pararm event: The event to trigger
        @type event: QtGui.QPaintEvent
        """

        self.editor.lineNumberAreaPaintEvent(event)