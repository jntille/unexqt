from PySide2 import QtWidgets, QtGui, QtCore


from unexQt.widgets.USpinbox.USpinboxBase import UIntChannelSpinbox, UDoubleChannelSpinbox, UMultiChannelDial
from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class URGBIntDials(UMultiChannelDial):
    def __init__(self, parent=None):
        super(URGBIntDials, self).__init__(parent)

        self.r_dial = UIntChannelSpinbox()
        self.g_dial = UIntChannelSpinbox()
        self.b_dial = UIntChannelSpinbox()

        self.r_dial.setObjectName("red_channel")
        self.g_dial.setObjectName("green_channel")
        self.b_dial.setObjectName("blue_channel")

        self.add_channel(self.r_dial)
        self.add_channel(self.g_dial)
        self.add_channel(self.b_dial)


        self.color_picker = QtWidgets.QToolButton()
        self.color_picker.setIcon(icons.get_icon(material_icon_tokens.COLORIZE))
        self.color_picker.setCheckable(True)
        self.layout().addWidget(self.color_picker)

class URGBDoubleDials(UMultiChannelDial):
    def __init__(self, parent=None):
        super(URGBDoubleDials, self).__init__(parent)
        self.r_dial = UDoubleChannelSpinbox()
        self.g_dial = UDoubleChannelSpinbox()
        self.b_dial = UDoubleChannelSpinbox()

        self.r_dial.setObjectName("red_channel")
        self.g_dial.setObjectName("green_channel")
        self.b_dial.setObjectName("blue_channel")

        self.add_channel(self.r_dial)
        self.add_channel(self.g_dial)
        self.add_channel(self.b_dial)

        self.color_picker = QtWidgets.QToolButton()
        self.color_picker.setIcon(icons.get_icon(material_icon_tokens.COLORIZE))
        self.color_picker.setCheckable(True)
        self.layout().addWidget(self.color_picker)
