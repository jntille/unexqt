from PySide2 import QtWidgets, QtGui, QtCore

class UIntChannelSpinbox(QtWidgets.QDoubleSpinBox):
    def __init__(self, parent=None):
        super(UIntChannelSpinbox, self).__init__(parent)
        self.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
        self.setStepType(QtWidgets.QAbstractSpinBox.DefaultStepType)

class UDoubleChannelSpinbox(QtWidgets.QDoubleSpinBox):
    def __init__(self, parent=None):
        super(UDoubleChannelSpinbox, self).__init__(parent)
        self.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
        self.setStepType(QtWidgets.QAbstractSpinBox.DefaultStepType)

class UMultiChannelDial(QtWidgets.QWidget):
    valueChanged = QtCore.Signal(tuple)

    def __init__(self, parent=None):
        super(UMultiChannelDial, self).__init__(parent)
        main_layout = QtWidgets.QHBoxLayout()
        self.setLayout(main_layout)

        self.channel_dials = []

    def get_channel_widget(self, index:int):
        return self.channel_dials[index]

    def setValue(self, value: tuple):
        for i, v in enumerate(value):
            self.channel_dials[i].setValue(v)
    
    def value(self) -> tuple:
        return (channel.value() for channel in self.channel_dials)
    
    def setEnabled(self, enabled):
        for channel in self.channel_dials:
            channel.setEnabled(enabled)

    def add_channel(self, channel_widget, channel_label:str=None):
        if channel_label is not None:
            self.layout().addWidget(QtWidgets.QLabel(channel_label))

        channel_widget.valueChanged.connect(lambda: self.valueChanged.emit(self.value()))
        self.channel_dials.append(channel_widget)
        self.layout().addWidget(channel_widget)
