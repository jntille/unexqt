from PySide2 import QtWidgets, QtGui, QtCore

class USpinboxDial(QtWidgets.QDoubleSpinBox):
    def __init__(self, parent=None):
        super(USpinboxDial, self).__init__(parent)

        self.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
        self.setStepType(QtWidgets.QAbstractSpinBox.DefaultStepType)

    # adjust step precision dynamically based on cursor position
    def setAdaptiveStepValue(currentStepValue):
        pass
        # self.setSingleStep()