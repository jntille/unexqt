from PySide2 import QtWidgets, QtGui, QtCore
from unexQt.widgets.USpinbox.USpinboxBase import UIntChannelSpinbox, UDoubleChannelSpinbox, UMultiChannelDial

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens


class UCropbox(UMultiChannelDial):
    def __init__(self, parent=None):
        super(UCropbox, self).__init__(parent)

        self.x_dial = UIntChannelSpinbox()
        self.y_dial = UIntChannelSpinbox()
        self.w_dial = UIntChannelSpinbox()
        self.h_dial = UIntChannelSpinbox()

        self.add_channel(self.x_dial)
        self.add_channel(self.y_dial)
        self.add_channel(self.w_dial)
        self.add_channel(self.h_dial)

        self.crop_region_button = QtWidgets.QToolButton()
        self.crop_region_button.setIcon(icons.get_icon(material_icon_tokens.CROP))
        self.crop_region_button.setCheckable(True)
        self.layout().addWidget(self.crop_region_button)
