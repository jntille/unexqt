from PySide2 import QtCore, QtGui, QtWidgets

class GenericLineDialog(QtWidgets.QDialog):

    def __init__(self, windowTitle):
        super(GenericLineDialog, self).__init__()
        
        self.setWindowTitle(windowTitle)
                
        QBtn = QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel
        
        self.buttonBox = QtWidgets.QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.lineEdit = QtWidgets.QLineEdit()

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.lineEdit)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

    def getText(self):
        return self.lineEdit.text()

    def accept(self):
        return super().accept()

    def reject(self):
        return super().reject()

class AddTabDialog(GenericLineDialog):
    def __init__(self):
        super(AddTabDialog, self).__init__("Add tab")
