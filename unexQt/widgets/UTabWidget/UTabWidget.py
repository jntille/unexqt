 
from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.UTabWidget.dialogs import AddTabDialog
from unexQt.widgets.UButton.UStandardButton import UAddButton


class URenameableTabWidget(QtWidgets.QTabWidget):
    tabAdded = QtCore.Signal(int)
    createTab = QtCore.Signal()

    def __init__(self, parent=None):
        super(URenameableTabWidget, self).__init__(parent)

        self.setTabBar(RenameableTabBar())

        # add tab button
        self.addTabBtn = UAddButton()
        self.setCornerWidget(self.addTabBtn)
        
        # config
        self.setFocusPolicy(QtCore.Qt.NoFocus)

        # Signals
        self.addTabBtn.clicked.connect(self.createTab.emit)
        self.tabCloseRequested.connect(self.on_tab_close_requested)
    
    def on_tab_close_requested(self, index):
        if 0 < index < self.tabBar().count():
            self.widget(index).deleteLater()
            self.removeTab(index)

class RenameableTabBar(QtWidgets.QTabBar):
    tabRenamed = QtCore.Signal(int)

    def __init__(self, parent=None):
        super(RenameableTabBar, self).__init__(parent)
        self.setTabsClosable(True)
        self.setMovable(True)

        # Signals
        self.tabBarDoubleClicked.connect(self.start_rename)

    def rename_tab(self, tabIndex, oldTabText, newTabText):
        self.setTabText(tabIndex, newTabText)
        self.tabRenamed.emit(tabIndex)

    def get_tabs(self):
        tabList = []
        for index in range(self.count()):
            tabList.append(self.tabText(index))
        return tabList
    
    def get_tab_index_by_text(self, text):
        for index in range(self.count):
            if self.tabText(index) == text:
                return index

    # INSIDE TAB LINE EDIT RENAME
    def start_rename(self, tab_index):
        # https://stackoverflow.com/questions/30269046/inline-renaming-of-a-tab-in-qtabbar-qtabwidget
        oldTabText = self.tabText(tab_index)
        rect = self.tabRect(tab_index)
        self.__lineEdit = TabLineEdit(oldTabText, rect, parent=self) 
        self.__lineEdit.editingFinished.connect(lambda: self.finish_rename(tab_index, oldTabText, newTabText = self.__lineEdit.text()))

    def finish_rename(self, index, oldTabText, newTabText):
        if newTabText not in self.get_tabs():
            self.rename_tab(index, oldTabText, newTabText)
        self.__lineEdit.hide()
        self.__lineEdit.deleteLater()

class TabLineEdit(QtWidgets.QLineEdit):
    """Temporary lineEdit to edit a tab"""
    def __init__(self, prev_text, rect, parent):
        super(TabLineEdit, self).__init__(parent)
        self.prev_text = prev_text
        top_margin = 3
        left_margin = 6
        self.move(rect.left() + left_margin, rect.top() + top_margin)
        self.resize(rect.width() - 2 * left_margin, rect.height() - 2 * top_margin)
        self.setText(prev_text)
        self.selectAll()
        self.setFocus()
        self.show()

    def keyPressEvent(self, event):
        pressedKey = event.key()
        if pressedKey == QtCore.Qt.Key_Escape:
            self.setText(self.prev_text) # reset text
            self.clearFocus()
        super(TabLineEdit, self).keyPressEvent(event)