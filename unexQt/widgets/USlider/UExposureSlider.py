 
 
from PySide2 import QtWidgets, QtGui, QtCore

from unexQt.widgets.USlider.USpinboxSlider import UDoubleSpinboxSlider
from unexQt.widgets.USlider.utils import reparameterize

class UExposureSlider(UDoubleSpinboxSlider):
    def __init__(self, parent=None):
        super(UExposureSlider, self).__init__(parent=parent)   
        self.setRange(-5,5)
        self.setTickInterval(1)
        self.setTickPosition(QtWidgets.QSlider.TickPosition.TicksBothSides)

        # self.get_slider().setRange(-5.6, 5.6)
        # self.get_spinbox().setRange(-5.6, 5.6)

    def from_slider_to_spinbox_value(self, value:int):
        # from int to 0-1 range
        span = self.getRange()
        float_value = reparameterize(self.slider.minimum(), self.slider.maximum(), span[0], span[1])(value)
        return float_value


    def from_spinbox_to_slider_value(self, value:float):
        span = self.getRange()
        int_value = reparameterize(span[0], span[1], self.slider.minimum(), self.slider.maximum())(value)
        return int(int_value)