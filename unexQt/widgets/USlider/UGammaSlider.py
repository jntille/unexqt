import math
from PySide2 import QtWidgets, QtGui, QtCore

from unexQt.widgets.USlider.USpinboxSlider import UDoubleSpinboxSlider
from unexQt.widgets.USlider.utils import reparameterize


class UGammaSlider(UDoubleSpinboxSlider):
    def __init__(self, parent=None):
        super(UGammaSlider, self).__init__(minimum=0, maximum=4, parent=parent)


    def from_slider_to_spinbox_value(self, value:int):
        # from int to 0-1 range
        spinbox_value = reparameterize(self.slider.minimum(), self.slider.maximum(), 0, 1)(value)
        
        # look up value on exp curve
        a = 1
        b = 4
        base = 2
        x_offset = -1
        y_offset = 0
            
        log_curve = lambda x: a*math.pow(2, (x+x_offset)*b) + y_offset
        log_curve_zero_to_one = reparameterize(log_curve(0),1,0,1)

        difference = lambda x: log_curve(x) - log_curve_zero_to_one(log_curve(x))
        blend_mix = lambda x: -4*math.pow((x-0.5), 2)+1 if x <= 0.5 else 1

        blended_curve = lambda x: log_curve_zero_to_one(log_curve(x)) + blend_mix(x)*difference(x)

        return reparameterize(0,1,0,4)(blended_curve(spinbox_value))

    def from_spinbox_to_slider_value(self, value:float):
        zeto_to_one_value = reparameterize(0,4,0,1)(blended_curve(value))

        return 1