 
from PySide2 import QtWidgets, QtGui, QtCore

from unexQt.widgets.USlider.utils import reparameterize

class UIntSpinboxSlider(QtWidgets.QWidget):
    valueChanged = QtCore.Signal(int)

    def __init__(self, parent=None):
        super(UIntSpinboxSlider, self).__init__(parent)
        main_layout = QtWidgets.QHBoxLayout()
        main_layout.setContentsMargins(0,0,0,0)
        self.setLayout(main_layout)

        self.slider = QtWidgets.QSlider()
        self.slider.setOrientation(QtCore.Qt.Horizontal)

        self.spinbox = QtWidgets.QSpinBox()

        main_layout.addWidget(self.slider)
        main_layout.addWidget(self.spinbox)

        self.slider.valueChanged.connect(lambda value: self.spinbox.setValue(value))
        self.slider.valueChanged.connect(lambda value: self.valueChanged.emit(value))

    def setMaximum(self, maximum):
        self.slider.setMaximum(maximum)
        self.spinbox.setMaximum(maximum)

    def get_value(self):
        return self.slider.value()
    
    def set_value(self, value:int):
        self.slider.setValue(value)
        self.spinbox.setValue(value)


class UDoubleSpinboxSlider(QtWidgets.QWidget):
    valueChanged = QtCore.Signal(float)

    def __init__(self, minimum=0, maximum=1, default_value=0 ,parent=None):
        super(UDoubleSpinboxSlider, self).__init__(parent)
    
        # members
        self._default_value = default_value
        
        # layout 
        main_layout = QtWidgets.QHBoxLayout()
        main_layout.setContentsMargins(0,0,0,0)
        self.setLayout(main_layout)

        self.slider = QtWidgets.QSlider()
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        
        self.spinbox = QtWidgets.QDoubleSpinBox()

        main_layout.addWidget(self.slider)
        main_layout.addWidget(self.spinbox)
        
        # signals
        self.slider.valueChanged.connect(lambda value: self.from_slider_to_spinbox_value(value))
        self.spinbox.valueChanged.connect(lambda value: self.from_spinbox_to_slider_value(value))

        # initial state
        self.setSliderResolution(4096) # how many integer steps
        self.setRange(minimum, maximum)

        self.spinbox.setValue(self._default_value)
        self.from_spinbox_to_slider_value(self._default_value)

    def setTickInterval(self, tickInterval:float):
        _min , _max = self.getRange()
        self.slider.setTickInterval(self._slider_resolution/(_max-_min))

    def setTickPosition(self, tickPosition:QtWidgets.QSlider.TickPosition):
        self.slider.setTickPosition(tickPosition)

    def setSliderResolution(self, resolution:int):
        self._slider_resolution = resolution
        self.slider.setRange(0, self._slider_resolution)

    def setRange(self, minimum:float, maximum:float):
        self._span = (minimum, maximum)
        self.spinbox.setRange(minimum, maximum)

    def getRange(self):
        return self._span
        
    def getValue(self):
        return self.spinbox.value()
    
    def setValue(self, value):
        self.slider.setValue(value)
        self.spinbox.setValue(value)

    def from_slider_to_spinbox_value(self, value:int):
        float_value = reparameterize(
            self.slider.minimum(), self.slider.maximum(), 
            self._span[0], self._span[1])(value) # from int to float

        if float_value != self.spinbox.value():
            self.spinbox.setValue(float_value)
            self.valueChanged.emit(self.getValue())

    def from_spinbox_to_slider_value(self, value:float):
        int_value = reparameterize(
            self._span[0], self._span[1],
            self.slider.minimum(), self.slider.maximum(), 
            )(value) # from int to float
        
        if int_value != self.slider.value():
            self.slider.setValue(int_value)
            self.valueChanged.emit(self.getValue())

    def get_slider(self):
        return self.slider
    
    def get_spinbox(self):
        return self.spinbox
