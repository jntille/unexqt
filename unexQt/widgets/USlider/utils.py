 
def reparameterize(old_min, old_max, new_min, new_max):
    def new_value(old_value):
        return ((old_value - old_min) / (old_max - old_min)) * (new_max - new_min) + new_min
    return new_value

