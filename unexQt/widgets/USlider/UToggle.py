from PySide2 import QtWidgets, QtCore, QtGui

class UToggle(QtWidgets.QSlider):
    """On/Off Toggle switch"""

    def __init__(self, *args, **kwargs):
        super(UToggle, self).__init__(QtCore.Qt.Horizontal, *args, **kwargs)
        self.state = False
        self.setMinimum(0)
        self.setMaximum(1)
        self.setSingleStep(1)
        self.setupConnections()
        self.setProperty("state", False)

    def setupConnections(self):
        self.valueChanged.connect(self.update_style)
        self.sliderPressed.connect(self.toggle)

    def update_style(self):
        """Action when the toggle is dragged."""
        if self.value() == 1:
            self.setProperty("state", True)
        else:
            self.setProperty("state", False)
        # redraw 
        self.style().unpolish(self)
        self.style().polish(self)
        self.update()

    def toggle(self):
        """Action when the toggle is clicked.
        This trigger the change action as well."""
        if self.value() == 0:
            self.setValue(1)
        else:
            self.setValue(0) 

if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    widget = UToggle()
    
    widget.show()
    app.exec_()