from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UTextEdit import UTextEdit

class LogTextEdit(UTextEdit.UTextEdit):
    matchCounterChanged = QtCore.Signal(tuple)

    def __init__(self, parent):
        """
        """
        super(LogTextEdit, self).__init__(parent)
