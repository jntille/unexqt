import logging
import re
from PySide2 import QtWidgets, QtCore, QtGui

# TODO: what did logging helpers do? fix
from unexQt.widgets.ULogView import logging_helpers

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class LogLvlFilter(QtWidgets.QToolBar):
    filterChanged = QtCore.Signal(tuple)
    def __init__(self):
        super(LogLvlFilter, self).__init__()
        
        self.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)

        self.ALL_ICON = icons.get_icon(material_icon_tokens.ALL_INCLUSIVE)
        self.CRITICAL_ICON = icons.get_icon(material_icon_tokens.DO_NOT_DISTURB)
        self.ERROR_ICON = icons.get_icon(material_icon_tokens.ERROR)
        self.WARNING_ICON = icons.get_icon(material_icon_tokens.WARNING)
        self.INFO_ICON = icons.get_icon(material_icon_tokens.INFO)
        self.DEBUG_ICON = icons.get_icon(material_icon_tokens.HELP)

        self.ALL_ACTION = self.addAction(self.ALL_ICON, 'all')
        self.CRITICAL_ACTION = self.addAction(self.CRITICAL_ICON, 'crit')
        self.CRITICAL_ACTION.setData(logging.CRITICAL)
        self.ERROR_ACTION = self.addAction(self.ERROR_ICON, 'error')
        self.ERROR_ACTION.setData(logging.ERROR)
        self.WARNING_ACTION = self.addAction(self.WARNING_ICON, 'warn')
        self.WARNING_ACTION.setData(logging.WARNING)
        self.INFO_ACTION = self.addAction(self.INFO_ICON, 'info')
        self.INFO_ACTION.setData(logging.INFO)
        self.DEBUG_ACTION = self.addAction(self.DEBUG_ICON, 'debug')
        self.DEBUG_ACTION.setData(logging.DEBUG)
        self.currentActiveLogLevels = logging_helpers.get_log_levels()
            
        self.ALL_ACTION.triggered.connect(lambda checked: self.changeLogLvlFilter(checked, self.ALL_ACTION))
        self.CRITICAL_ACTION.triggered.connect(lambda checked: self.changeLogLvlFilter(checked, self.CRITICAL_ACTION))
        self.ERROR_ACTION.triggered.connect(lambda checked: self.changeLogLvlFilter(checked, self.ERROR_ACTION))
        self.WARNING_ACTION.triggered.connect(lambda checked: self.changeLogLvlFilter(checked, self.WARNING_ACTION))
        self.INFO_ACTION.triggered.connect(lambda checked: self.changeLogLvlFilter(checked, self.INFO_ACTION))
        self.DEBUG_ACTION.triggered.connect(lambda checked: self.changeLogLvlFilter(checked, self.DEBUG_ACTION))

        for action in self.actions():
            action.setCheckable(True)

        self.ALL_ACTION.setChecked(True)
        self.changeLogLvlFilter(True, self.ALL_ACTION)

        self.filterChanged.connect(lambda logLevels: print("current LogLevels: ", logLevels))

    def changeLogLvlFilter(self, checked, action):
        """One of the most stupid methods

        Args:
            checked ([type]): [description]
            action ([type]): [description]
        """
        # all_Action pressed
        if action == self.ALL_ACTION:
            if checked:
                checkedActions = self.getCheckedActions()
                checkedActions.remove(self.ALL_ACTION)
                for logLevelAction in checkedActions:
                    logLevelAction.setChecked(False)

                self.currentActiveLogLevels = logging_helpers.get_log_levels() 
                self.filterChanged.emit(self.logLevels())
                return self.logLevels()
            else:
                self.ALL_ACTION.setChecked(True)
                return self.logLevels()
        # logLevel_Action pressed
        else:
            self.ALL_ACTION.setChecked(False)

            if checked == True:
                self.currentActiveLogLevels.clear()
                checkedActions = self.getCheckedActions()
                for logLevelAction in checkedActions:
                    self.currentActiveLogLevels.add(logLevelAction.data())
                self.filterChanged.emit(self.logLevels())
                return self.logLevels()
            else:
                checkedActions = self.getCheckedActions()
                if len(checkedActions) > 0:
                    action.setChecked(False)
                    self.currentActiveLogLevels.remove(action.data())
                    self.filterChanged.emit(self.logLevels())
                    return self.logLevels()
                else:
                    self.ALL_ACTION.setChecked(True)
                    self.changeLogLvlFilter(True, self.ALL_ACTION)

    def logLevels(self):
        return tuple(self.currentActiveLogLevels)
    
    def getCheckedActions(self):
        logLevelActions = self.actions()
        checkedActions = []
        for logLevelAction in logLevelActions:
            if logLevelAction.isChecked():
                checkedActions.append(logLevelAction)
        return checkedActions
            
    def validate(self, parsedMessage:dict):
        if parsedMessage is not None:
            logLevel = parsedMessage['levelname']
            if logLevel in self.currentActiveLogLevels:
                return True
            else:
                return False
        else:
            # message couldn't be parsed
            return False

class DateTimeRange(QtWidgets.QWidget):
    filterChanged = QtCore.Signal(int)

    def __init__(self):
        super(DateTimeRange, self).__init__()
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        self.lay.setContentsMargins(0,0,0,0)
        self.setSizePolicy(QtWidgets.QSizePolicy.Policy.Minimum, QtWidgets.QSizePolicy.Policy.Preferred)

        self.enableFilterChkbox = QtWidgets.QCheckBox()
        self.lay.addWidget(self.enableFilterChkbox)

        self.fromLabel = QtWidgets.QLabel("from: ")
        self.toLabel = QtWidgets.QLabel("  to: ")

        self.fromDate= QtWidgets.QDateEdit(QtCore.QDate.currentDate())
        self.fromDate.setCalendarPopup(True)
        self.fromDate.setDisplayFormat("dd.MM.yyyy")
        self.fromTime = QtWidgets.QTimeEdit()
        self.fromTime.setDisplayFormat("hh:mm")
        self.toDate = QtWidgets.QDateEdit(QtCore.QDate.currentDate())
        self.toDate.setCalendarPopup(True)
        self.toDate.setDisplayFormat("dd.MM.yyyy")
        self.toTime = QtWidgets.QTimeEdit()
        self.toTime.setDisplayFormat("hh:mm")

        self.lay.addWidget(self.fromLabel)
        self.lay.addWidget(self.fromDate)
        self.lay.addWidget(self.fromTime)
        self.lay.addWidget(self.toLabel)
        self.lay.addWidget(self.toDate)
        self.lay.addWidget(self.toTime)

        self.enableFilterChkbox.stateChanged.connect(self.enableFilter)

    def enableFilter(self, state):
        widgets = [self.lay.itemAt(i) for i in range(self.lay.count())]
        widgets.remove(self.enableFilterChkbox)
        if state == QtCore.Qt.Unchecked:
            for widget in widgets:
                widget.setEnabled(False)
        elif state == QtCore.Qt.Checked:
            for widget in widgets:
                widget.setEnabled(True)
    
    def validate(self, parsedMessage:dict):
        return True
