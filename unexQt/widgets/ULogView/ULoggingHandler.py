import logging
import datetime
import parser
import re
from PySide2 import QtWidgets, QtCore, QtGui
FALLBACK_LOGFORMAT = '%(asctime)s  [%(levelname)s\t]  in %(filename)s %(funcName)s: %(message)s'
FALLBACK_DATEFORMAT = "%Y-%m-%d %H:%M:%S"

def compileMsgRegex(msgFormat, dateFormat, regexDict):
    # escape characters that fuck up the regex
    msgFormat = msgFormat.replace(r'\t', '\s')
    msgFormat = msgFormat.replace('[', '\[')
    msgFormat = msgFormat.replace(']', '\]')

    # to parse out information we replace the format string keywords with regexes 
    regex = msgFormat % regexDict
    regex = regex.replace(' ', r'\s')

    return re.compile(regex)
    

def parseMessage(msg:str, regexObj, dateFormat) -> dict:
    """parse a log message with the help of regex

    Args:
        msg (str): [description]
        msgFormat (str): [description]
        dateFormat (str): [description]

    Returns:
        dict: [description]
    """
    # match
    match = regexObj.match(msg)
    if match is not None:
        matchDict = match.groupdict()
        # additional casting 
        castedDict = {
            'asctime': datetime.datetime.strptime(
                matchDict['asctime'],
                dateFormat),
            'levelname': logging._checkLevel(matchDict['levelname'])
        }
        matchDict.update(castedDict)
        return matchDict
    else:
        return None

class ULogRecordEmitter(QtCore.QObject):
    # a helper object that allows the log handler to use the qt Signal / Slot system
    appendLine = QtCore.Signal(str)

    def __init__(self):
        super(ULogRecordEmitter, self).__init__()

class ULoggingHandler(logging.Handler):
    def __init__(self, 
            level:int = logging.DEBUG,
            msgFormat:str = FALLBACK_LOGFORMAT,
            dateFormat:str = FALLBACK_DATEFORMAT):

        super(ULoggingHandler, self).__init__()
        
        self.msgFormat = msgFormat
        self.dateFormat = dateFormat

        self.formatter = logging.Formatter(self.msgFormat, self.dateFormat)
        self.setFormatter(self.formatter)

        self.regexDict = {
            'asctime': r'(?P<asctime>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})',
            'levelname': r'(?P<levelname>[a-zA-Z]+)',
            'filename': r'(?P<filename>.+\.\w+)',
            'funcName': r'(?P<funcName>\w+)',
            'message': r'(?P<message>.+)'
        }
        self.regexObj = compileMsgRegex(msgFormat, dateFormat, self.regexDict)
        self.logEmitter = ULogRecordEmitter()

    def emit(self, record):
        msg = self.format(record)
        self.getEmitter().appendLine.emit(msg)

    def getEmitter(self):
        return self.logEmitter
    
