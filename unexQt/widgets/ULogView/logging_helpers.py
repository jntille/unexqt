import logging

def get_log_levels():
    return set(logging._levelToName.keys())

