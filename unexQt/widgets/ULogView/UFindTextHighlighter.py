from PySide2 import QtCore, QtGui, QtWidgets
import re

class UFindTextHighlighter(QtCore.QObject):
    matchesChanged = QtCore.Signal()

    def __init__(self, parent:QtGui.QTextDocument):
        super(UFindTextHighlighter, self).__init__()
        self._document = parent

        self.default_format = QtGui.QTextCharFormat()
        self.default_format.setFont(parent.defaultFont())

        self.highlight_format = QtGui.QTextCharFormat()
        self.highlight_format.setFont(parent.defaultFont())
        self.highlight_format.setBackground(QtCore.Qt.red)

        self.highlight_cursor = QtGui.QTextCursor(self.document())

        self._matches = []
        self.current_regex = None

    def set_highlight_format(self, format:QtGui.QTextCharFormat):
        self.highlight_format = format

    def document(self):
        return self._document

    def set_regex(self, pattern:str, options:QtCore.QRegularExpression.PatternOptions):
        self._matches = []
        self.current_regex = QtCore.QRegularExpression(pattern, options) if pattern !="" else None
        self.rehighlightDocument()
    
    def get_matches(self):
        return self._matches
    
    def get_match_with_index(self, index:int):
        if self._matches:
            return self._matches[index]
        else:
            return None
            
    def get_match_start_pos(self, index):
        match = self.get_match_with_index(index)
        if match != None:
            return match[0]

    def get_match_length(self, index):
        match = self.get_match_with_index(index)
        if match != None:
            return match[1]

    def get_total_match_count(self):
        return len(self._matches)
    
    def rehighlightDocument(self):
        self.clearHighlighted()
        self._matches = []
        if self.current_regex != None:
            regex_iter = self.current_regex.globalMatch(self.document().toPlainText())
            while (regex_iter.hasNext()):
                match = regex_iter.next()
                start = match.capturedStart()
                length = match.capturedLength()
                self._matches.append((start, length))
                self.highlightTextInRange(start, length)
        print(self._matches)
        self.matchesChanged.emit()

    def highlightTextInRange(self, start, length):
            self.highlight_cursor.setPosition(start)
            self.highlight_cursor.movePosition(QtGui.QTextCursor.Right,
                                                QtGui.QTextCursor.KeepAnchor,
                                                length)
            self.highlight_cursor.setCharFormat(self.highlight_format)

    def clearHighlighted(self):
        self.highlight_cursor.setPosition(QtGui.QTextCursor.Start)
        self.highlight_cursor.movePosition(QtGui.QTextCursor.End, mode=QtGui.QTextCursor.KeepAnchor)
        self.highlight_cursor.setCharFormat(self.default_format)
        self.highlight_cursor.clearSelection()