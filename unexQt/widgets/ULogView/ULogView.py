import logging

from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.ULogView import ULogFilter, ULogTextEdit, LineNumberArea, UFindTextHighlighter
from unexQt.widgets.UCollapsibleBox import UCollapsibleBox
from unexQt.widgets.ULogView import ULogFilterWidgets
from unexQt.widgets.ULineEdit import UFindTextPatternLine

import logging
import re
from unexQt.widgets.ULogView import ULoggingHandler
from unexQt.widgets.ULogView import logging_helpers

class ULogView(QtWidgets.QWidget):
    """
    The entire log widget
    """

    logFilterChanged = QtCore.Signal()

    def __init__(self, parent=None):
        super(ULogView, self).__init__(parent)
        
        self._matches = []
        self._last_search_case_stv = False
        self._search_text = ''
        self._first_visible_index = 0
        self._last_visible_index = 0
        self._matches_to_highlight = []
        self._current_match = 0

        self.setup_layout()
        self.setup_signals()
        self.setup_logging()

        self.demo()


    def setup_layout(self):
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.setContentsMargins(0,0,0,0)
        self.setLayout(mainLayout)

        # textedit
        self.logTextEdit = ULogTextEdit.LogTextEdit(self)
        mainLayout.addWidget(self.logTextEdit)

        self.find_text_highlighter = UFindTextHighlighter.UFindTextHighlighter(self.logTextEdit.proxy_document)

        # filter :w
        # widgets
        firstFilterLine = QtWidgets.QHBoxLayout()
        firstFilterLine.setContentsMargins(0,0,0,0)
        mainLayout.addLayout(firstFilterLine)

        self.logLevelFilter = ULogFilterWidgets.LogLvlFilter()
        firstFilterLine.addWidget(self.logLevelFilter, alignment=QtCore.Qt.AlignLeft)

        self.textSearchLine = UFindTextPatternLine.UFindTextPatternLine()
        firstFilterLine.addWidget(self.textSearchLine, alignment=QtCore.Qt.AlignRight)

        # more filters
        # self.advancedFilterBox = UCollapsibleBox.UCollapsibleBox()
        # mainLayout.addWidget(self.advancedFilterBox, alignment=QtCore.Qt.AlignTop)
        # advancedFilterLayout = QtWidgets.QVBoxLayout()

        # self.dateTimeRangeFilter = ULogFilterWidgets.DateTimeRange()
        # advancedFilterLayout.addWidget(self.dateTimeRangeFilter)
        # self.advancedFilterBox.setContentLayout(advancedFilterLayout)
    
    def setup_signals(self):
        self.logLevelFilter.filterChanged.connect(self.updateProxyDocument)
#        self.dateTimeRangeFilter.filterChanged.connect(self.updateProxyDocument)
        
        # connecting searchbox with the logEdit
        self.textSearchLine.searchChanged.connect(
            lambda: self.find_text_highlighter.set_regex(
                pattern=self.textSearchLine.get_pattern(),
                options=self.textSearchLine.get_flags()
            )            
        )
        self.find_text_highlighter.matchesChanged.connect(
            lambda: self.textSearchLine.set_current_match_state(
                index=0 if self.find_text_highlighter.get_total_match_count() > 0 else -1,
                total_match_number=self.find_text_highlighter.get_total_match_count()
            )
        )
        self.textSearchLine.currentMatchIndexChanged.connect(lambda index: self.logTextEdit._move_to_match(
            pos=self.find_text_highlighter.get_match_start_pos(index),
            length=self.find_text_highlighter.get_match_length(index)
        ))



    def setup_logging(self, logger=logging.getLogger(), handler=ULoggingHandler.ULoggingHandler(), handlerLevel=logging.DEBUG):
        # connect to logger
        self.logger = logger
        self.handler = handler
        self.handler.setLevel(handlerLevel)
        self.logger.addHandler(self.handler)
        self.proxyLogLevel = logging_helpers.get_log_levels()

        # always log to master
        self.handler.getEmitter().appendLine.connect(lambda msg: self.logTextEdit.appendMessage(msg, self.logTextEdit.master_document))
        # send for proxy validation
        self.handler.getEmitter().appendLine.connect(lambda msg: self.appendMessageToProxy(msg))
    
    def validateAgainstFilters(self, msg:str):
        """Validate if a message is acceptable to display given the current filter settings

        Args:
            msg ([type]): [description]

        Returns:
            [type]: [description]
        """
        parsedMessage = ULoggingHandler.parseMessage(
            msg,
            self.handler.regexObj,
            self.handler.dateFormat 
        )

        results = []
        results.append(self.logLevelFilter.validate(parsedMessage))
        #results.append(self.dateTimeRangeFilter.validate(parsedMessage))
        return all(results)

    def updateProxyDocument(self):
        """recalculates the entire proxy document for the current filter settings
        """
        self.logTextEdit.proxy_document.clear()
        currentBlock = self.logTextEdit.master_document.begin()
        while currentBlock.isValid():
            msg = currentBlock.text()
            self.appendMessageToProxy(msg)
            currentBlock = currentBlock.next()
    
    def appendMessageToProxy(self, msg):
        if self.validateAgainstFilters(msg) == True:
            self.logTextEdit.appendMessage(msg, self.logTextEdit.proxy_document)


    def demo(self):
        for i in range(0,200):
            logging.debug('damn, a bug')
            logging.info('current state is ...')
            logging.warning('something is not right')
            logging.error('couldn\'t connect to ...')
            logging.critical('crashed')