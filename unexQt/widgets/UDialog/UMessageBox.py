 
class CueMessageBox(QtWidgets.QMessageBox):
    ''' Display QMessageBox with message and OK button.
        @type message: str
        @param message: error message
        @type title: str
        @param title: box title
        @type parent: QWidget
        @param parent: parent object, used for centering, deleting
        @type centerOnScreen: bool
        @param centerOnScreen: useful mainly for rare cases that parent is not shown yet for centering on desktop
                              If parent is shown,  QMessageBox gets centered into it properly.
    '''
    def __init__(self, message, title=None, parent=None):
        super(CueMessageBox, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Information)
        self.setText(message)
        self.setWindowTitle(title)
        self.setStandardButtons(QtWidgets.QMessageBox.Ok)

    def centerOnScreen(self):
        ''' Useful mainly for rare cases that parent is not shown yet for centering on desktop
                              If parent is shown,  QMessageBox gets centered into it properly.'''
        size = self.size()
        desktopSize = QtWidgets.QDesktopWidget().screenGeometry()
        top = (desktopSize.height() / 2) - (size.height() / 2)
        left = (desktopSize.width() / 2) - (size.width() / 2)
        self.move(left, top)
