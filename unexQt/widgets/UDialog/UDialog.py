from PySide2 import QtWidgets, QtCore, QtGui
from unexQt.styling import unexQt


class UDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(UDialog, self).__init__(parent)
        self.setStyleSheet(unexQt.load_stylesheet_pyside2())