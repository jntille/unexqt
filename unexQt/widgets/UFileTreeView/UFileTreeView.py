import pathlib
import os

from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UFileTreeView import UFileTreeModel
from unexQt.widgets.UFileTreeView.UFileSystemNode import Node, RootNode, DirNode, FileNode
from unexQt.widgets.UButton import UFileDialogButton

class UFileTreeView(QtWidgets.QTreeView):
    nodesSelected = QtCore.Signal(list)

    def __init__(self, model=None, parent=None):
        super(UFileTreeView, self).__init__(parent)

        self.tree_model = model if model is not None else UFileTreeModel.UFileTreeModel()
        self.setModel(self.tree_model)

        # Defaults
        self.header().setSectionResizeMode(0, QtWidgets.QHeaderView.Interactive)
        for i in range(1, len(self.model().get_header_columns())):
            self.header().setSectionResizeMode(i, QtWidgets.QHeaderView.Stretch)
        self.header().setSortIndicatorShown(True)

        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setAlternatingRowColors(True)
1