import pathlib
import os

def log(node, tabLevel=-1):
        output     = ""
        tabLevel += 1
        for i in range(tabLevel):
            output += "\t"
        output += "|------" + str(type(node)) + "\n"
        for child in node._children:
            output += log(child, tabLevel)
        tabLevel -= 1
        output += "\n"
        
        return output

class Node(object):
    def __init__(self, parent=None):
        self._children = []
        self._parent = parent
        self._data = None
        if parent is not None:
            parent.add_child(self)

    def data(self):
        return self._data

    def set_data(self, data):
        self._data = data

    def add_child(self, child):
        self._children.append(child)

    def removeChild(self, child):
        self._children.remove(child) 

    def child_count(self):
        return len(self._children)

    def parent(self):
        return self._parent

    def children(self):
        return self._children
    
    def child(self, row):
        return self._children[row]

    def row(self):
        if self._parent is not None:
            return self._parent._children.index(self)

    def __str__(self):
        return "<{}> with children {}".format(type(self), str(self._children))

    def get_child_nodes_of_type(self, classType):
        nodeList = []
        for child in self.children():
           if isinstance(child, classType):
               nodeList.append(child)
        return nodeList

class RootNode(Node):
    def __init__(self):
        super(RootNode, self).__init__(parent=None)

    def get_dir_node_children(self):
        return self.get_child_nodes_of_type(DirNode)


class DirNode(Node):
    def __init__(self, path: pathlib.Path, data: dict={}, parent=None):
        super(DirNode, self).__init__(parent)
        self._path = path
        self._data = data

    def path(self):
        return self._path

    def set_path(self, path: pathlib.Path):
        # retrieve data
        self._path = str(path)

    def set_data(self, data: dict):
        # retrieve path
        self._data = data

    def getFileNodeChildren(self):
        return self.get_child_nodes_of_type(FileNode)

    def removeChildren(self):
        self._children.clear()

class FileNode(Node):
    def __init__(self, path: pathlib.Path, data: dict={}, parent=None):
        super(FileNode, self).__init__(parent)
        self._path = path
        self._data = data

    def path(self):
        return self._path

    def set_path(self, path: pathlib.Path):
        self._path = path

    def set_data(self, data: dict):
        self._data = data

    # FileNode are always terminal nodes, no children allowed
    def add_child(self, child):
        pass

    def child_count(self):
        return 0

    def child(self, row):
        return None

    def children(self):
        return None

