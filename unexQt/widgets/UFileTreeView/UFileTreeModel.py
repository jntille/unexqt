from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UFileTreeView.UFileSystemNode import RootNode, DirNode, FileNode

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UFileTreeModel(QtCore.QAbstractItemModel):
    def __init__(self, root_node: RootNode = RootNode(), header_columns: list = ['file'], parent=None):
        super(UFileTreeModel, self).__init__(parent)
        
        self._root_node = root_node
        self._header_columns = header_columns
        self.FOLDER_ICON = icons.get_icon(material_icon_tokens.FOLDER_OPEN)
        self.FILE_ICON = icons.get_icon(material_icon_tokens.FILE_PRESENT)

    def get_header_columns(self):
        return self._header_columns
        
    def get_root_node(self):
        return self._root_node

    def set_root_node(self, root_node):
        self.beginResetModel()
        self._root_node = root_node
        self.endResetModel()

    def rowCount(self, parent):
        if not parent.isValid():
            parent_node = self._root_node
        else:
            parent_node = parent.internalPointer()
        return parent_node.child_count()

    def columnCount(self, parent):
        return len(self._header_columns)

    def data(self, index, role):
        if not index.isValid():
            return None
        node = index.internalPointer()
        
        if role == QtCore.Qt.DisplayRole: 
            if index.column() == 0: # corresponds to first header section
                if isinstance(node, DirNode):
                    return str(node.path())
                elif isinstance(node, FileNode):
                    if isinstance(node.parent(), DirNode):
                        return node.path().name
                    else:
                        return str(node.path())
            else:
                # look whether nodes data dict contains an entry for a header section
                return node.data().get(self._header_columns[index.column()], '')

        # show folder or image icon in the first section
        elif role == QtCore.Qt.DecorationRole:
            if index.column() == 0:
                if isinstance(node, DirNode):
                    return self.FOLDER_ICON
                elif isinstance(node, FileNode):
                    return self.FILE_ICON

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            return self._header_columns[section]

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable 

    # returns parent (wrapped in a modelindex) of a given index 
    def parent(self, index):
        node = self.get_node(index)
        parent_node = node.parent()
        if parent_node == self._root_node:
            return QtCore.QModelIndex()
        return self.createIndex(parent_node.row(), 0, parent_node)

    # return a child a given row, column of a given parent node 
    def index(self, row, column, parent):
        parent_node = self.get_node(parent)
        childItem = parent_node.child(row)

        if childItem:
            return self.createIndex(row, column, childItem)
        else: # if childItem None
            return QtCore.QModelIndex()

    def get_node(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node
        # is rootnode with invalid modelindex
        else:
            return self._root_node

    def insert(self, pathList):
        # generate list of already existing dir nodes
        print("insert called with: {}".format(pathList))
        if isinstance(pathList, list) and len(pathList) > 0:
            if len(pathList) == 1:
                for path in pathList:
                    existingDirs = set([dirNode.path() for dirNode in self.get_root_node().get_dir_node_children()])

                    if path.is_dir():
                        self.beginResetModel()
                        DirNode(path, parent=self.get_root_node())
                        self.endResetModel()

                    elif path.is_file():
                        if path.parent in existingDirs:
                            self.beginResetModel()
                            DirNode(path, parent=self.get_root_node())
                            self.endResetModel()
                        else:
                            self.beginResetModel()
                            parentDir = DirNode(path.parent, parent=self.get_root_node())
                            FileNode(path, parent=parentDir)
                            self.endResetModel()

            else:
                tempDict = {}
                for path in pathList:
                    if path.parent in list(tempDict.keys()):
                        value = tempDict[path.parent]
                        value.append(path)
                        tempDict[path.parent] = value 
                    else:
                        childFiles = [path]
                        tempDict[path.parent] = childFiles

                self.beginResetModel()
                for key, value in tempDict.items():
                    parent_node = DirNode(key, parent=self.get_root_node())
                    for filepath in value:
                        FileNode(filepath, parent=parent_node)
                self.endResetModel()
                #! possibly list of directories?
        
    def remove_selected_nodes(self, selectedIndexes): #of column 0
        dir_node_list = []
        file_node_list = []
        for index in selectedIndexes:
            node =  index.internalPointer() 
            if isinstance(node, FileNode):
                file_node_list.append(node)
            elif isinstance(node, DirNode):
                dir_node_list.append(node)

        # one must first detach and delete filenodes before deleting their parent directory node
        self.beginResetModel()
        for node in file_node_list:
            parent = node.parent()
            parent.removeChild(node)
            del node

        for node in dir_node_list:
            node.removeChildren()
            parent = node.parent()
            parent.removeChild(node)
            del node
        self.endResetModel()

    def get_all_files(self):
        for node in self._root_node._children:
            if isinstance(node, DirNode):
                # determine if use global settings of folder settings and extract arguments
                fileDict[str(node.path)] = {
                        "imgFiles": node.getAllImages(),
                }
