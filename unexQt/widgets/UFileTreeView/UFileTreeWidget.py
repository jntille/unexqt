
from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UFileTreeView import UFileTreeModel, UFileTreeView
from unexQt.widgets.UFileTreeView.UFileSystemNode import Node, RootNode, DirNode, FileNode
from unexQt.widgets.UButton import UFileDialogButton

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UFileTreeWidget(QtWidgets.QWidget):
    def __init__(self,
            fileMode: QtWidgets.QFileDialog.FileMode = QtWidgets.QFileDialog.ExistingFiles
            ):
        super(UFileTreeWidget, self).__init__()
        self.setMinimumHeight(80)
        self.vlay = QtWidgets.QVBoxLayout()
        self.vlay.setContentsMargins(0,0,0,0)
        self.setLayout(self.vlay)

        self.file_tree_view = UFileTreeView.UFileTreeView()
        self.vlay.addWidget(self.file_tree_view)

        self.below_view_lay = QtWidgets.QHBoxLayout()
        self.vlay.addLayout(self.below_view_lay)
        self.below_view_lay.setContentsMargins(0,0,0,0)

        self.view_status_widget = QtWidgets.QWidget()
        self.view_status_lay = QtWidgets.QHBoxLayout()
        self.view_status_widget.setLayout(self.view_status_lay)
        self.below_view_lay.addWidget(self.view_status_widget, alignment = QtCore.Qt.AlignLeft)

        self.add_remove_widget = QtWidgets.QWidget()
        self.add_remove_lay = QtWidgets.QHBoxLayout()
        self.add_remove_widget.setLayout(self.add_remove_lay)
        self.add_remove_lay.setContentsMargins(0,0,0,0)
        self.below_view_lay.addWidget(self.add_remove_widget, alignment = QtCore.Qt.AlignRight)

        self.add_btn = UFileDialogButton.UFileDialogButton()
        self.add_btn.get_dialog().setFileMode(fileMode)
        self.remove_btn = QtWidgets.QToolButton()
        self.remove_btn.setIcon(icons.get_icon(material_icon_tokens.REMOVE))
        self.add_remove_lay.addWidget(self.remove_btn)
        self.add_remove_lay.addWidget(self.add_btn)

        # Signals
        self.add_btn.filesSelected.connect(self.file_tree_view.model().insert)
        self.remove_btn.clicked.connect(lambda: self.file_tree_view.model().removeSelectedNodes(self.file_tree_view.selectionModel().selectedRows(column=0)))

