 
from PySide2 import QtCore, QtGui, QtWidgets
from unexQt.widgets.UMainWindow import UMainWindow

from unexQt.widgets.UListView import UAbstractITDModel, UAbstractITDDelegate

class UAbstractITDListView(QtWidgets.QListView):

    def __init__(self):
        super(UAbstractITDListView, self).__init__()
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.setItemDelegate(UAbstractITDDelegate.UAbstractITDDelegate())

if __name__ == "__main__":
    app = QtWidgets.QApplication([])

    window = UMainWindow.UMainWindow()
    widget = UAbstractITDListView()
    window.setCentralWidget(widget)
    
    window.show()
    app.exec_()


    
# # related links
# # https://stackoverflow.com/questions/63497841/qlistwidget-does-not-resize-itself/63502112#63502112
# class USearchListView(QtWidgets.QListView):
#     itemExecuted = QtCore.Signal(QtCore.QModelIndex)
#     focusBack = QtCore.Signal(QtGui.QKeyEvent)

#     def __init__(self, parent=None):
#         super(USearchListView, self).__init__(parent)

#         # variables 
#         self.max_items_to_display = 3

#         #setup the source model with a proxy model used to filter/sort it
#         self._source_model = QtCore.QStringListModel()
#         self._proxy_model = QtCore.QSortFilterProxyModel()
#         self._proxy_model.setSourceModel(self._source_model)
#         self.setModel(self._proxy_model)

#         #configure proxymodel
#         self._proxy_model.setDynamicSortFilter(True)

#         # configure listview
#         self.setTabKeyNavigation(False) # use tab for search execution instead 
#         self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
#         self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
#         # must be set so that qabstractscroll area's sizehint() method doesn't default to some random size
#         self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
#         self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
#         self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
#         self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
#         self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)

#         # SIGNALS
#         self.clicked.connect(lambda index: self.itemExecuted[QtCore.QModelIndex].emit(index))

#     def set_max_items_to_display(self, max_items:int):
#         self.max_items_to_display = max_items

#     def set_source_model(self, src_model:QtCore.QAbstractListModel):
#         self._source_model = src_model
#         self._proxy_model.setSourceModel(self._source_model)

#     def get_proxy_model(self):
#         return self._proxy_model
        
#     def set_proxy_model(self, proxy_model):
#         self._proxy_model = proxy_model
#         self._proxy_model.setSourceModel(self._source_model)
#         self.setModel(self._proxy_model)

#     def set_proxy_filter(self, regex):
#         self._proxy_model.setFilterRegularExpression(regex)

#         # search has changed, which invalidates the currentIndex and selection
#         self.clearSelection()
#         self.setCurrentIndex(self.model().index(-1,-1))
#         if self.model().rowCount() > 0:
#             self.setCurrentIndex(self.model().index(0,0))
#         self.updateGeometry()
    
#     def keyPressEvent(self, event):
#         pressedKey = event.key()
#         if pressedKey == QtCore.Qt.Key_Up:
#             self.move_to_next_index()
#         elif pressedKey == QtCore.Qt.Key_Down:
#             self.move_to_prev_index()
#         elif pressedKey in [QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter]:
#             self.itemExecuted[QtCore.QModelIndex].emit(self.currentIndex())
#         else:
#             #super(UListSearchView, self).keyPressEvent(event)
#             self.focusBack.emit(event)
#             self.clearFocus()
    
#     def move_index_by_pressed_key(self, pressedKey:QtGui.QKeyEvent):
#         pressedKey = pressedKey.key()
#         if pressedKey == QtCore.Qt.Key_Up:
#             self.move_to_prev_index()
#         elif pressedKey == QtCore.Qt.Key_Down:
#             self.move_to_next_index()
    
#     def move_to_prev_index(self):
#         index = self.currentIndex()
#         if index.isValid():
#             if index.row() == 0: # last item reached jump back to top
#                 self.setCurrentIndex(self.model().index(self.model().rowCount()-1,0))
#             else:
#                 self.setCurrentIndex(self.moveCursor(QtWidgets.QAbstractItemView.MoveUp, QtCore.Qt.NoModifier))
#         else:
#             if self.model().rowCount() > 0:
#                 self.setCurrentIndex(self.model().index(0,0))

#     def move_to_next_index(self):
#         index = self.currentIndex()
#         if index.isValid():
#             if index.row() == self.model().rowCount()-1: # last item reached jump back to top
#                 self.setCurrentIndex(self.model().index(0,0))
#             else:
#                 self.setCurrentIndex(self.moveCursor(QtWidgets.QAbstractItemView.MoveDown, QtCore.Qt.NoModifier))
#         else:
#             if self.model().rowCount() > 0:
#                 self.setCurrentIndex(self.model().index(0,0))

#     # must be overridden as there seems to be a default arbitrary minimumSizeHint() 
#     def minimumSizeHint(self):
#         return QtCore.QSize(-1,-1)

#     def viewportSizeHint(self):
#         item_number = min(self.max_items_to_display, self.model().rowCount())
#         total_width = super().viewportSizeHint().width()
        
#         if item_number > 0:
#             total_spacing = self.spacing() * item_number * 2 #empty space that is padded around an item in the layout. 
#             total_height = sum(self.sizeHintForRow(i) for i in range(item_number)) + total_spacing
#             return QtCore.QSize(total_width, total_height)
#         else:
#             return QtCore.QSize(total_width, 0)

#     def sizeHint(self):
#         if self.model().rowCount() > 0:
#             return super(USearchListView, self).sizeHint()
#         else:
#             return QtCore.QSize(0, 0)

