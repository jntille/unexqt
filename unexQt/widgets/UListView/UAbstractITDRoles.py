
from PySide2 import QtCore

# Custom Roles for data retrieval
TitleRole = QtCore.Qt.UserRole + 1
DescriptionRole = QtCore.Qt.UserRole + 2
ImageRole = QtCore.Qt.UserRole + 3
EntireDataRole = QtCore.Qt.UserRole + 4