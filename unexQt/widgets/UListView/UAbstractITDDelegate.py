import copy

from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.UListView.UAbstractITDRoles import TitleRole, DescriptionRole, ImageRole
from unexQt.styling.utils import icons

from unexQt.utils.color import color_convert
from unexQt.widgets.ULabel import UElidedLabel

class UAbstractITDDelegate(QtWidgets.QStyledItemDelegate):
    """ A generic Icon Title Description (ITD) Delegate """
    def __init__(self):
        super(UAbstractITDDelegate, self).__init__()

        # color
        self.colors = {
            QtWidgets.QStyle.State_None: {
                "title_text": color_convert.hex_to_qcolor("#FFFFFF"),
                "description_text": color_convert.hex_to_qcolor("#BBBBBB"),
                "sunken": color_convert.hex_to_qcolor("#282828"),
            },
            QtWidgets.QStyle.State_Selected: {
                "title_text": color_convert.hex_to_qcolor("#FFFFFF"),
                "description_text": color_convert.hex_to_qcolor("#FFFFFF"),
                "item_bg": color_convert.hex_to_qcolor("#1464a0"),
                "sunken": color_convert.hex_to_qcolor("#125b91"),
            }
        }

        # dimension vars
        self.item_padding = 2 # border around itemedge
        self.image_scale = .5
        self.title_padding = 2
        self.description_padding = 2
        self.title_description_spacing = 2
        self.image_title_spacing = 4
        self.image_description_spacing = 4
        self.corner_radius = 4
        self.set_description_line_count(1)

        self.set_title_font(
            family="Sans Serif",
            pointSize=8,
            weight=QtGui.QFont.Bold
        )
        self.set_description_font(
            family="Sans Serif",
            pointSize=8,
            weight=QtGui.QFont.Normal
        )

    def set_title_padding(self, padding:int):
        self.description_padding = padding

    def set_description_padding(self, padding:int):
        self.title_padding = padding

    def set_title_font(self, family, pointSize, weight):
        self.title_font = QtGui.QFont()
        self.title_font.setFamily(family)
        self.title_font.setPointSize(pointSize)
        self.title_font.setWeight(weight)

        self.title_font_metric =  QtGui.QFontMetrics(self.title_font)

    def set_description_font(self, family, pointSize, weight):
        self.description_font = QtGui.QFont()
        self.description_font.setFamily(family)
        self.description_font.setPointSize(pointSize)
        self.description_font.setWeight(weight)
        
        self.description_font_metric =  QtGui.QFontMetrics(self.description_font)

    def set_description_line_count(self, line_count:int):
        self.description_line_count = line_count

    def get_title(self, index) -> str:
        return index.data(TitleRole)

    def get_description(self, index) -> str:
        return index.data(DescriptionRole)
    
    def get_icon(self, index):
        return index.data(ImageRole)

    def paint_icon(self, painter, option, index) -> QtCore.QRect:
        icon = self.get_icon(index)
        mode = icons.get_icon_mode(option.state)
        state = icons.get_icon_state(option.state)
        image = icon.pixmap(32, 32, mode, state).toImage()

        # backdrop
        aspect_ratio = image.width() / image.height() 
        image_bg_rect = QtCore.QRect(
            option.rect.x() + self.item_padding,
            option.rect.y() + self.item_padding,
            option.rect.height()*aspect_ratio - 2*self.item_padding,
            option.rect.height() - 2*self.item_padding,
        )
        painter.drawRoundedRect(image_bg_rect, self.corner_radius, self.corner_radius)

        # icon
        image_offset = int((image_bg_rect.height() - image_bg_rect.height() * self.image_scale) / 2)
        image_rect = image_bg_rect.adjusted(
            image_offset,
            image_offset,
            -image_offset,
            -image_offset
        )
        painter.drawImage(image_rect, image)

        return image_bg_rect

    def get_title_bg_height(self) -> int:
        return self.title_font_metric.height() + self.title_padding*2

    def paint_title(self, painter, option, index, image_bg_rect) -> QtCore.QRect:
        painter.save()
        state = self.get_state(option)
        title = self.get_title(index)

        # backdrop
        width = min(
            option.rect.width() - (2*self.item_padding + image_bg_rect.width() + self.image_description_spacing),
            self.title_font_metric.boundingRect(title).width() + 2*self.title_padding
        )
        title_bg_rect = QtCore.QRect(
            option.rect.x() + self.item_padding + image_bg_rect.width() + self.image_title_spacing,
            option.rect.y() + self.item_padding,
            width,
            self.get_title_bg_height(),
        )
        painter.drawRoundedRect(title_bg_rect, self.corner_radius, self.corner_radius)

        # text
        title_rect = title_bg_rect.adjusted(
            self.title_padding,
            self.title_padding,
            -self.title_padding,
            -self.title_padding
        )
        elided_title_text = self.title_font_metric.elidedText(title, QtCore.Qt.ElideRight, title_rect.width())
        painter.setPen(QtGui.QPen(self.colors[state]["title_text"]))
        painter.setFont(self.title_font)
        painter.drawText(title_rect, QtCore.Qt.TextSingleLine, elided_title_text)
        painter.restore()
        return title_bg_rect

    def get_description_bg_height(self) -> int:
        return  self.description_line_count * self.description_font_metric.height() + (self.description_line_count-1) * self.description_font_metric.leading() + 2*self.description_padding

    def paint_description(self, painter, option, index, image_bg_rect, title_bg_rect) -> QtCore.QRect:
        painter.save()
        state = self.get_state(option)
        description = self.get_description(index)

        # backdrop
        # to draw the bg rect for the description, one must first determine the hight of the linewrapped text bbox
        description_bg_rect = QtCore.QRect(
            option.rect.x() + self.item_padding + image_bg_rect.width() + self.image_description_spacing, 
            option.rect.y() + self.item_padding + title_bg_rect.height() + self.title_description_spacing,
            option.rect.width() - (2*self.item_padding + image_bg_rect.width() + self.image_description_spacing),
            self.get_description_bg_height()
        )            
        painter.drawRoundedRect(description_bg_rect, self.corner_radius, self.corner_radius)

        # text
        description_text_rect = description_bg_rect.adjusted(
            self.description_padding,
            self.description_padding,
            -self.description_padding,
            -self.description_padding
        )
        painter.setPen(QtGui.QPen(self.colors[state]["description_text"]))
        painter.setFont(self.description_font)
        painter.drawText(description_text_rect, QtCore.Qt.TextWordWrap, description)
        painter.restore()
        return description_bg_rect
    
    def get_state(self, option):
        if option.state & QtWidgets.QStyle.State_Selected:
            return QtWidgets.QStyle.State_Selected
        else:
            return QtWidgets.QStyle.State_None

    def paint(self, painter:QtGui.QPainter, option:QtWidgets.QStyleOptionViewItem, index:QtCore.QModelIndex):
        painter.save()

        if index.column() == 0:
            painter.setRenderHint(QtGui.QPainter.RenderHint.Antialiasing, True)
            # colors based on item selection state
            state = self.get_state(option)
            if state == QtWidgets.QStyle.State_Selected:
                painter.fillRect(option.rect, self.colors[state]["item_bg"]) 
           
            # paint sunken backdrops
            painter.setPen(QtGui.QPen(QtCore.Qt.PenStyle.NoPen))
            painter.setBrush(QtGui.QBrush(self.colors[state]["sunken"], QtCore.Qt.BrushStyle.SolidPattern))

            image_bg_rect = self.paint_icon(painter, option, index)

            title_bg_rect = self.paint_title(painter, option, index, image_bg_rect)

            description_bg_rect = self.paint_description(painter, option, index, image_bg_rect, title_bg_rect)
        else:
            super().paint(painter, option, index)
        painter.restore()

    def get_item_height(self, option, index):
        title_height = self.get_title_bg_height()
        description_height = self.get_description_bg_height()
        return self.item_padding*2 + title_height + self.title_description_spacing + description_height

    def sizeHint(self, option, index:QtCore.QModelIndex):
        if index.isValid() == False:
            return QtCore.QSize()
        
        s = QtCore.QSize()
        s.setWidth(option.rect.width())
        s.setHeight(self.get_item_height(option, index)) 
        return s
    