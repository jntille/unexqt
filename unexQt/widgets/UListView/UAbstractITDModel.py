from PySide2 import QtCore, QtGui, QtWidgets

from unexQt.widgets.UListView.UAbstractITDRoles import TitleRole, DescriptionRole, ImageRole, EntireDataRole

class UAbstractITDModel(QtCore.QAbstractListModel):
    def __init__(self, model_data=[], parent=None):
        super(UAbstractITDModel, self).__init__(None)
        self.model_data = []

    def data(self, index, role):
        # translate index to dict key 
        data = self.model_data[index.row()]
        
        if role == TitleRole:
            return data.get("title")
        if role == DescriptionRole:
            return data.get("description")
        if role == ImageRole:
            image = QtWidgets.QFileIconProvider().icon(QtWidgets.QFileIconProvider.IconType.Folder).pixmap(QtCore.QSize(32,32)).toImage()
            return image
        if role == EntireDataRole:
            return data

    def rowCount(self, index):
        return len(self.model_data)

    def flags(self, index):
        return QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    def set_model_data(self, model_data):
        self.model_data = model_data






