from PySide2 import QtWidgets, QtCore, QtGui
from unexQt.styling import unexQt


class UMainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(UMainWindow, self).__init__(parent)
        self.setStyleSheet(unexQt.load_stylesheet_pyside2())