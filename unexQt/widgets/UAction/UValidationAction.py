 
from PySide2 import QtWidgets, QtGui

from unexQt.widgets.UAction import constants

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UValidationAction(QtWidgets.QAction):
    def __init__(self, default_state:QtGui.QValidator.State = QtGui.QValidator.State.Intermediate, parent=None):
        super(UValidationAction, self).__init__(parent)
        self.setObjectName(constants.VALIDATION_ACTION_OBJECTNAME)
        self.set_show_pending_icon(True)

        self.set_pending_icon(QtGui.QIcon())
        self.set_valid_icon(icons.get_icon(material_icon_tokens.CHECK))
        self.set_invalid_icon(icons.get_icon(material_icon_tokens.WARNING))

        self.setEnabled(True)
        self.setVisible(True)
        self.set_state(default_state)

    def set_valid_icon(self, icon):
        self.VALID_ICON = icon
        
    def set_invalid_icon(self, icon):
        self.INVALID_ICON = icon

    def set_pending_icon(self, icon):
        self.PENDING_ICON = icon
        
    def set_show_pending_icon(self, show_pending_icon:bool):
        self._show_pending_icon = show_pending_icon

    def generateTooltipText(self, validators):
        pass

    def set_state(self, state:QtGui.QValidator.State):
        self._state = state
        if state == QtGui.QValidator.Invalid:
            self.setIcon(self.INVALID_ICON)
            self.setVisible(True)
        elif state == QtGui.QValidator.Intermediate:
            if self._show_pending_icon == True:
                self.setIcon(self.PENDING_ICON)
            else:
                self.setVisible(False)
        elif state == QtGui.QValidator.Acceptable:
            self.setVisible(True)
            self.setIcon(self.VALID_ICON)