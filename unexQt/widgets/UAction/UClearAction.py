from PySide2 import QtWidgets

from unexQt.widgets.UAction import constants

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UClearAction(QtWidgets.QAction):
    def __init__(self, parent=None):
        super(UClearAction, self).__init__(parent)
        self.setEnabled(True)
        self.setVisible(True)
        self.setObjectName(constants.CLEAR_ACTION_OBJECTNAME)
        self.setIcon(icons.get_icon(material_icon_tokens.CLEAR))

    def dynamicVisibility(self, text):
        if len(text) > 0:
            self.setVisible(True)
        elif len(text) < 1:
            self.setVisible(False)
