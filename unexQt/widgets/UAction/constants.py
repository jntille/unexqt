from PySide2.QtWidgets import QAction

SEARCH_ACTION_OBJECTNAME = "qlineeditsearchaction"
CLEAR_ACTION_OBJECTNAME = "qlineeditclearaction"
FILTER_ACTION_OBJECTNAME = "qlineeditfilteraction"
VALIDATION_ACTION_OBJECTNAME = "qlineeditvalidationaction"

def get_action_from_widget(widget, action_object_name:str):
    action = widget.findChild(QAction, action_object_name)
    return action
