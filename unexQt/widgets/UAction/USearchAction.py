from PySide2 import QtWidgets

from unexQt.widgets.UAction import constants

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class USearchAction(QtWidgets.QAction):
    def __init__(self, parent=None):
        super(USearchAction, self).__init__(parent)
        self.setEnabled(True)
        self.setObjectName(constants.SEARCH_ACTION_OBJECTNAME)
        self.setIcon(icons.get_icon(material_icon_tokens.SEARCH))