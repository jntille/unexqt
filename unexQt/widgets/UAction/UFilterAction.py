from PySide2 import QtWidgets

from unexQt.widgets.UAction import constants

from unexQt.styling.utils import icons
from unexQt.styling.unexQt.rc import material_icon_tokens

class UFilterAction(QtWidgets.QAction):
    def __init__(self, parent=None):
        super(UFilterAction, self).__init__(parent)
        self.setEnabled(True)
        self.setVisible(True)
        self.setObjectName(constants.FILTER_ACTION_OBJECTNAME)
        self.setIcon(icons.get_icon(material_icon_tokens.FILTER_ALT))