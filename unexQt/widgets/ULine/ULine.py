from PySide2 import QtWidgets, QtCore, QtGui

class UHLine(QtWidgets.QFrame):
    """A simple horizontal line."""
    def __init__(self):
        super(UHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Plain)
        
        self.setFixedHeight(1)


class UVLine(QtWidgets.QFrame):
    """A simple horizontal line."""
    def __init__(self):
        super(UVLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.VLine)
        self.setFrameShadow(QtWidgets.QFrame.Plain)
        
        self.setFixedHeight(1)
