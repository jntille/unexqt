from PySide2.QtGui import QColor


def hex_to_rgb(hex):
    hex = hex.lstrip('#')
    hlen = len(hex)

    return tuple(int(hex[i:i+2], 16) for i in range(0, hlen, hlen // 3))

def hex_to_qcolor(hex):
    color = hex_to_rgb(hex)
    return rgb_to_qcolor(color)

def rgb_to_qcolor(color:tuple):
    if len(color) in range(3,5):
        qcolor = QColor()
        red = color[0] 
        green = color[1]
        blue = color[2]

        # differentiate between int und float rgb
        if isinstance(red, int):
            qcolor.setRed(red)
            qcolor.setGreen(green)
            qcolor.setBlue(blue)
            # alpha
            if len(color) == 3:
                qcolor.setAlpha(255)
            elif len(color) == 4:
                qcolor.setAlpha(color[3])

        elif isinstance(red, float):
            qcolor.setRedF(red)
            qcolor.setGreenF(green)
            qcolor.setBlueF(blue)
            # alpha
            if len(color) == 3:
                qcolor.setAlphaF(1)
            elif len(color) == 4:
                qcolor.setAlphaF(color[3])
        return qcolor
