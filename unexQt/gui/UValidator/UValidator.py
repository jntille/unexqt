import pathlib

from PySide2 import QtWidgets, QtCore, QtGui

# additional common properties for a validator
class UValidator(object):
    validated = QtCore.Signal(QtGui.QValidator.State) # emitted by validate()

    def __init__(self, parent=None):
        super(UValidator, self).__init__(parent)
        self._state = None

    def set_state(self, state):
        self._state = state

    def get_state(self):
        return self._state

    def get_tooltip(self):
        return None

# combines multiple validators into one
class UValidatorCollection(QtGui.QValidator, UValidator):
    def __init__(self, validators: list = []):
        super(UValidatorCollection, self).__init__()
        self._validators = validators

    def get_validators(self):
        return self._validators

    def add_validator(self, validator):
        self._validators.append(validator) 

    def add_validators(self, validatorList):
        self._validators.extend(validatorList) 

    def validate(self, _input: str, pos: int) -> QtGui.QValidator.State:
        texts = []
        positions = []
        collective_state = QtGui.QValidator.Acceptable

        for validator in self._validators:
            testState = validator.validate(_input, pos)
            print(type(testState), testState)
            if testState == QtGui.QValidator.Invalid:
                collective_state = QtGui.QValidator.Invalid
            elif (testState == QtGui.QValidator.Intermediate and collective_state != QtGui.QValidator.Invalid):
                collective_state = QtGui.QValidator.Intermediate
        self.validated.emit(collective_state)
        return collective_state

    def get_tooltip(self):
        tooltip_text = "validation:"
        for validator in self._validators:
            tooltip = validator.get_tooltip()
            if tooltip:
                tooltip_text = tooltip_text + r"\n" + "\t" + tooltip 



class ASCIIValidator(QtGui.QRegExpValidator, UValidator):
    def __init__(self):
        super(ASCIIValidator, self).__init__()

        self.regex = QtCore.QRegExp(r"[^\x00-\x7F]")
        self.regex.setCaseSensitivity(QtCore.Qt.CaseInsensitive)

        self.setRegExp(self.regex)
    
    def validate(self, _input: str, pos: int) -> QtGui.QValidator.State:
        if _input == "":
            self._state = QtGui.QValidator.Intermediate
            return QtGui.QValidator.Intermediate
        
        charOffset = 0 # -1 if there was no further match found
        matchCount = 0
        matchList = []
        while(self.regExp().indexIn(_input, charOffset) != -1):
            matchCount += 1
            matchList.append(self.regExp().cap(1))
            charOffset += self.regExp().matchedLength()

        if matchCount > 0: # means that a non-ascii character has been foound
            self._state = QtGui.QValidator.Invalid
            return QtGui.QValidator.Invalid
        else:
            self._state = QtGui.QValidator.Acceptable
            return QtGui.QValidator.Acceptable
    
    def get_tooltip(self):
        state = self.get_state()
        if state == QtGui.QValidator.State.Acceptable:
            return "passes ASCII validator"
        elif state == QtGui.QValidator.State.Invalid:
            return "failed ASCII validator"
        
class UDirpathValidator(QtGui.QValidator, UValidator):
    def __init__(self):
        super(UDirpathValidator, self).__init__()

    def validate(self, _input: str, pos: int) -> QtGui.QValidator.State:
        path = pathlib.Path(_input)
        if path.exists() == True:
            if path.is_dir() == True:
                return QtGui.QValidator.Acceptable
            else:
                return QtGui.QValidator.Invalid
        else:
            return QtGui.QValidator.Invalid

class UFilepathValidator(QtGui.QValidator, UValidator):
    def __init__(self):
        super(UFilepathValidator, self).__init__()

    def validate(self, _input: str, pos: int) -> QtGui.QValidator.State:
        path = pathlib.Path(_input)
        if path.exists() == True:
            if path.is_file() == True:
                return QtGui.QValidator.Acceptable
            else:
                return QtGui.QValidator.Invalid
        else:
            return QtGui.QValidator.Invalid

