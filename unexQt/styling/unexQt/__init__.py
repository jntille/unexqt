#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Standard library imports
import pathlib

from unexQt.styling.utils import stylesheet
from unexQt.styling.utils import color_schemes

# globals 
STYLESHEET_DIR = pathlib.Path(__file__).parent
STYLESHEET_NAME = STYLESHEET_DIR.name

COLOR_SCHEME = color_schemes.get_color_scheme_from_name("dark")

def load_stylesheet(qt_api):
    return stylesheet.load_stylesheet(
        qt_api=qt_api, 
        stylesheet=STYLESHEET_DIR, 
        color_scheme=COLOR_SCHEME
    )

def load_stylesheet_pyside():
    return load_stylesheet(qt_api='pyside')

def load_stylesheet_pyside2():
    return load_stylesheet(qt_api='pyside2')

def load_stylesheet_pyqt():
    return stylesheet.load_stylesheet(qt_api='pyqt4')

def load_stylesheet_pyqt5():
    return stylesheet.load_stylesheet(qt_api='pyqt5')
