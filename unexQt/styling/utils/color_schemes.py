import pathlib
import json

from PySide2 import QtCore, QtGui

COLOR_SCHEMES_DIR = pathlib.Path(__file__).parent.parent / "color_schemes"

def get_color_schemes() -> list:
    color_scheme_files = [] 
    for scheme_file in COLOR_SCHEMES_DIR.glob('**/*.json'):
        color_scheme_files.append(scheme_file)
    return color_scheme_files

def get_color_scheme_from_file(scheme_file:pathlib.Path) -> dict:
    with open(str(scheme_file), "r") as read_file:
        color_scheme_data = json.load(read_file)

    color_scheme_dict = {
            "name": scheme_file.stem,
            "file": scheme_file,
            "data": color_scheme_data
    }
    return color_scheme_dict

def get_color_scheme_from_name(scheme_name:str) -> dict:
    for scheme_file in COLOR_SCHEMES_DIR.glob('**/*.json'):
        if scheme_file.stem == scheme_name:
            return get_color_scheme_from_file(scheme_file)

def get_color_scheme_from_rc(rc_path:str) -> dict:
    json_file = QtCore.QFile(rc_path)
    json_file.open(QtCore.QIODevice.ReadOnly | QtCore.QIODevice.Text)
    byte_array = json_file.readAll()
    json_file.close()

    json_parse_error = QtCore.QJsonParseError()
    json_doc = QtCore.QJsonDocument.fromJson(byte_array, json_parse_error)

    if json_parse_error.error == QtCore.QJsonParseError.NoError:
        json_obj = json_doc.object()
        return json_obj




