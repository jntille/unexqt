import pathlib

def create_icon_tokens_file(tokens_file:pathlib.Path, root_dir:pathlib.Path, relative_dir:pathlib.Path, prefix:str):
    """assumes that svgs have been compiled to a rc file"""
    ICON_TOKEN_TEMPLATE = "{IDENTIFIER} = ':/{prefix}/{file}'\n"

    rel_location =  'img/' + str(relative_dir).rsplit(str(root_dir), 1)[-1][1:]

    if tokens_file.exists():
        tokens_file.unlink()

    with open(tokens_file, 'w') as f:
        token_lines = [ICON_TOKEN_TEMPLATE.format(
            IDENTIFIER=svg_file.stem.upper(),
            prefix=prefix,
            file="{rel_location}/{png_file}".format(rel_location=rel_location, png_file=svg_file.stem + ".png"),
        ) for svg_file in relative_dir.glob("*.svg")]

        f.writelines(token_lines)
