#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Standard library imports
import logging
import os
import platform
import warnings
import pathlib

from unexQt.styling.utils.rc import QT_API_TO_RC_FILE

STYLING_DIR = pathlib.Path(__file__).parent.parent

def get_stylesheets() -> list:
    EXCLUDE_DIRS = ['utils', 'color_schemes']
    stylesheets = []

    for stylesheet_dir in STYLING_DIR.iterdir():
        if stylesheet_dir.is_dir() and stylesheet_dir.name not in EXCLUDE_DIRS:
            stylesheets.append(stylesheet_dir)

    return stylesheets

def load_stylesheet(qt_api, stylesheet, color_scheme):
    """
    """
    stylesheet_name = stylesheet.stem

    if qt_api:
        os.environ['QT_API'] = qt_api

    # Import is made after setting QT_API
    from PySide2.QtCore import QCoreApplication, QFile, QTextStream
    from PySide2.QtGui import QColor, QPalette

    # Then we import resources - binary qrc content
    dynamic_import = "import unexQt.styling.{style}.rc.{scheme}.{style_rc}".format(
        style=stylesheet_name,
        scheme=color_scheme["name"],
        style_rc=QT_API_TO_RC_FILE[qt_api]
    )
    exec(dynamic_import)
    print(dynamic_import)

    # Thus, by importing the binary we can access the resources
    qss_rc_path = ":/{style_prefix}/style.qss".format(
        style_prefix=stylesheet_name
    )

    # It gets the qss file from compiled style_rc that was import
    # not from the file QSS as we are using resources
    qss_file = QFile(qss_rc_path)

    if qss_file.exists():
        qss_file.open(QFile.ReadOnly | QFile.Text)
        text_stream = QTextStream(qss_file)
        stylesheet = text_stream.readAll()
        print("QSS file sucessfuly loaded.")
    else:
        stylesheet = ""
        # Todo: check this raise type and add to docs
        raise FileNotFoundError("Unable to find QSS file '{}' "
                                "in resources.".format(qss_rc_path))

    # Todo: check execution order for these functions
    # 1. Apply OS specific patches
    stylesheet += _apply_os_patches(color_scheme["data"])

    # 2. Apply binding specific patches
    stylesheet += _apply_binding_patches()

    # 3. Apply binding version specific patches
    stylesheet += _apply_version_patches()

    # 4. Apply palette fix. See issue #139
    _apply_application_patches(QCoreApplication, QPalette, QColor, color_scheme["data"])
    return stylesheet

def _apply_os_patches(color_scheme:dict):
    """
    Apply OS-only specific stylesheet pacthes.

    Returns:
        str: stylesheet string (css).
    """
    os_fix = ""

    if platform.system().lower() == 'darwin':
        # See issue #12
        os_fix = '''
        QDockWidget::title
        {{
            background-color: {color};
            text-align: center;
            height: 12px;
        }}
        '''.format(color=color_scheme['COLOR_BACKGROUND_NORMAL'])

    # Only open the QSS file if any patch is needed
    if os_fix:
        _logger.info("Found OS patches to be applied.")

    return os_fix

def _apply_binding_patches():
    """
    Apply binding-only specific stylesheet patches for the same OS.

    Returns:
        str: stylesheet string (css).
    """
    binding_fix = ""

    if binding_fix:
        _logger.info("Found binding patches to be applied.")

    return binding_fix

def _apply_version_patches():
    """
    Apply version-only specific stylesheet patches for the same binding.

    Returns:
        str: stylesheet string (css).
    """
    version_fix = ""

    if version_fix:
        _logger.info("Found version patches to be applied.")

    return version_fix

def _apply_application_patches(QCoreApplication, QPalette, QColor, color_scheme:dict):
    """
    Apply application level fixes on the QPalette.

    The import names args must be passed here because the import is done
    inside the load_stylesheet() function, as QtPy is only imported in
    that moment for setting reasons.
    """
    # See issue #139
    color = QColor(color_scheme['COLOR_SELECTION_LIGHT'])

    # Todo: check if it is qcoreapplication indeed
    app = QCoreApplication.instance()

    print("Found application patches to be applied.")

    if app:
        app_palette = app.palette()
        app_palette.setColor(QPalette.Normal, QPalette.Link, color)
        app.setPalette(app_palette)
    else:
        print("No QCoreApplication instance found. "
                     "Application patches not applied. "
                     "You have to call load_stylesheet function after "
                     "instantiation of QApplication to take effect. ")


# test

# from PySide2.QtWidgets import QApplication
# app = QApplication([])
# stylesheet = load_stylesheet(
#     qt_api='pyside2',
#     stylesheet_name="unexQt",
#     color_scheme_name="dark"
# )
# print(stylesheet)