#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Utilities to process and convert svg images to png using palette colors.
"""

# Standard library imports
from __future__ import absolute_import, division, print_function
from subprocess import call

import os
import re
import pathlib
import shutil

# Third party imports
from PySide2 import QtWidgets, QtGui, QtCore

from unexQt.utils.color import color_convert

# Global variables
# to gen .qrc
TEMPLATE_QRC = '''<RCC warning="File created programmatically. All changes made in this file will be lost!">
{content}
</RCC>'''
TEMPLATE_QRC_RC_GROUP = '''  <qresource prefix="{prefix}">
{resource}  </qresource>'''
TEMPLATE_QRC_IMG_FILE = '    <file>{relative_location}/{filename}</file>'
TEMPLATE_QRC_QSS_FILE = '      <file>{filename}</file>'
TEMPLATE_QRC_COLOR_FILE = '      <file>{filename}</file>'

QT_API_TO_RC_FILE = {
        "qtpy" : "style_rc",
        "pyside" : "pyside_style_rc",
        "pyside2" : "pyside2_style_rc",
        "pyqt4" : "pyqt4_style_rc",
        "pyqt5" : "pyqt5_style_rc",
}

def _create_colored_png_from_svg(svg_file:pathlib.Path, png_file:pathlib.Path, color, width, height):
    """
    Convert svg files to png files using Qt.
    """

    svg_icon = QtGui.QIcon(str(svg_file))
    img = svg_icon.pixmap(QtCore.QSize(width, height)).toImage()

    painter = QtGui.QPainter()
    painter.begin(img)
    painter.setCompositionMode(QtGui.QPainter.CompositionMode_SourceIn)
    painter.fillRect(img.rect(), color)
    painter.end()

    img.save(str(png_file))


def create_images(src_dir:pathlib.Path, dest_dir:pathlib.Path, color_scheme:dict):
    """Create resources `rc` png image files from base svg files and palette.

    Search all SVG files in src_dir change its colors using color scheme for each
    state generating PNG images for each size `heights`.

    Args:
        base_svg_path (str, optional): [description]. Defaults to SVG_PATH.
        rc_path (str, optional): [description]. Defaults to RC_PATH.
        palette (DarkPalette, optional): Palette . Defaults to DarkPalette.
    """
    # Needed to use QPixmapapp = QApplication.instance()
    app = QtWidgets.QApplication.instance()
    if app == None:
        app = QtWidgets.QApplication([])

    # remove old
    if dest_dir.exists():
        shutil.rmtree(str(dest_dir))
        dest_dir.mkdir(parents=True)
    else:
        dest_dir.mkdir(parents=True)

    svg_files = [svg_file for svg_file in src_dir.glob("*.svg")]
    
    # extract icon colors
    color_disabled = color_convert.hex_to_qcolor(color_scheme['COLOR_BACKGROUND_NORMAL'])
    color_focus = color_convert.hex_to_qcolor(color_scheme['COLOR_SELECTION_LIGHT'])
    color_pressed = color_convert.hex_to_qcolor(color_scheme['COLOR_SELECTION_NORMAL'])
    color_normal = color_convert.hex_to_qcolor(color_scheme['COLOR_FOREGROUND_DARK'])

    # See: https://doc.qt.io/qt-5/scalability.html
    icon_sizes = {
        32: '.png',
        64: '@2x.png',
    }
    for size, ext in icon_sizes.items():
        for svg_file in svg_files:
            stem = svg_file.stem
            file_color_map = {
                    dest_dir / "{stem}{ext}".format(stem=stem, ext=ext)             : color_normal,
                    dest_dir / "{stem}_disabled{ext}".format(stem=stem, ext=ext)    : color_disabled,
                    dest_dir / "{stem}_focus{ext}".format(stem=stem, ext=ext)       : color_focus,
                    dest_dir / "{stem}_pressed{ext}".format(stem=stem, ext=ext)     : color_pressed,
            }

            for png_file, qcolor in file_color_map.items():
                _create_colored_png_from_svg(svg_file, png_file, qcolor, size, size)
    
def generate_qrc_string_for_icons(img_dir:pathlib.Path, relative_location:str, prefix:str):
    if img_dir.exists():
        # add imgages to .qrc
        img_files = sorted([img_file.name for img_file in img_dir.glob("*.png")])
        lines = [TEMPLATE_QRC_IMG_FILE.format(relative_location=relative_location, filename=img_file) + '\n' for img_file in img_files]
        img_rcs = "".join(lines)
        return TEMPLATE_QRC_RC_GROUP.format(prefix=prefix, resource=img_rcs)

def generate_qrc_string_for_qss(qss_file:pathlib.Path, prefix:str):
    if qss_file.exists():
        # add qss file to .qrc
        qss_rcs = TEMPLATE_QRC_QSS_FILE.format(filename=qss_file.name) + "\n"
        return TEMPLATE_QRC_RC_GROUP.format(prefix=prefix, resource=qss_rcs)

def generate_qrc_string_for_color_scheme(color_file:pathlib.Path, prefix:str):
    if color_file.exists():
        # add color file to .qrc
        color_rcs = TEMPLATE_QRC_COLOR_FILE.format(filename=color_file.name) + "\n"
        return TEMPLATE_QRC_RC_GROUP.format(prefix=prefix, resource=color_rcs)

def write_qrc_file(content:str, qrc_file:pathlib.Path):
    qrc_content = TEMPLATE_QRC.format(content=content)
    with open(str(qrc_file), 'w') as fh:
        fh.write(qrc_content)

def create_qrc_file(img_dir:pathlib.Path, color_file:pathlib.Path, qss_file:pathlib.Path, qrc_file:pathlib.Path, prefix:str):
    """
    Generate the QRC file programmaticaly.

    Search all RC folder for PNG images and create a QRC file.

    Args:
        resource_prefix (str, optional): Prefix used in resources.
            Defaults to 'qss_icons'.
        style_prefix (str, optional): Prefix used to this style.
            Defaults to 'qdarkstyle'.
    """
    content = []

    discovered_dirs = list(filter(lambda sub_dir: sub_dir.is_dir(), img_dir.iterdir()))
    if not discovered_dirs:
        discovered_dirs.append(img_dir)

    for dir in discovered_dirs:
        rel_location = str(dir).rsplit(str(img_dir.parent), 1)[-1][1:] # ugly but works
        content.append(generate_qrc_string_for_icons(dir, rel_location, prefix))

    content.append(generate_qrc_string_for_qss(qss_file, prefix))
    content.append(generate_qrc_string_for_color_scheme(color_file, prefix))
    
    write_qrc_file("".join(content), qrc_file)


def create_rcc(rc_dir:pathlib.Path, qrc_file:pathlib.Path, compile_for_lib:str):
    stem = qrc_file.stem
    ext = "rc.py"
    template = "{prefix}{stem}_{ext}".format(prefix='{prefix}', stem=stem, ext=ext)    
    py_file_pyqt5 = (rc_dir / template.format(prefix='pyqt5_'))
    py_file_pyqt = (rc_dir / template.format(prefix='pyqt_'))
    py_file_pyside = (rc_dir / template.format(prefix='pyside_'))
    py_file_pyside2 = (rc_dir / template.format(prefix='pyside2_'))
    py_file_qtpy = (rc_dir / template.format(prefix=''))
    py_file_pyqtgraph = (rc_dir / template.format(prefix='pyqtgraph'))

    # delete file if exists
    for compiled_file in [py_file_pyqt5, py_file_pyqt, py_file_pyside, py_file_pyside2, py_file_qtpy, py_file_pyqtgraph]:
        if compiled_file.exists():
            compiled_file.unlink()
    

    # calling external commands
    if compile_for_lib in ['pyqt', 'pyqtgraph', 'all']:
        print("Compiling for PyQt4 ...")
        try:
            call(['pyrcc4', '-py3', str(qrc_file), '-o', str(py_file_pyqt)])
        except FileNotFoundError:
            print("You must install pyrcc4")

    if compile_for_lib in ['pyqt5', 'qtpy', 'all']:
        print("Compiling for PyQt5 ...")
        try:
            call(['pyrcc5', str(qrc_file), '-o', str(py_file_pyqt5)])
        except FileNotFoundError:
            print("You must install pyrcc5")

    if compile_for_lib in ['pyside', 'all']:
        print("Compiling for PySide ...")
        try:
            call(['pyside-rcc', '-py3', str(qrc_file), '-o', str(py_file_pyside)])
        except FileNotFoundError:
            print("You must install pyside-rcc")

    if compile_for_lib in ['pyside2', 'all']:
        print("Compiling for PySide 2...")
        try:
            call(['pyside2-rcc', str(qrc_file), '-o', str(py_file_pyside2)])
        except FileNotFoundError:
            print("You must install pyside2-rcc")

    if compile_for_lib in ['qtpy', 'all']:
        print("Compiling for QtPy ...")
        # special case - qtpy - syntax is PyQt5
        with open(py_file_pyqt5, 'r') as file:
            filedata = file.read()

        # replace the target string
        filedata = filedata.replace('from PyQt5', 'from qtpy')

        with open(py_file_qtpy, 'w+') as file:
            # write the file out again
            file.write(filedata)

        if compile_for_lib not in ['pyqt5']:
            os.remove(py_file_pyqt5)

    if compile_for_lib in ['pyqtgraph', 'all']:
        print("Compiling for PyQtGraph ...")
        # special case - pyqtgraph - syntax is PyQt4
        with open(py_file_pyqt, 'r') as file:
            filedata = file.read()

        # replace the target string
        filedata = filedata.replace('from PyQt4', 'from pyqtgraph.Qt')

        with open(py_file_pyqtgraph, 'w+') as file:
            # write the file out again
            file.write(filedata)



#def create_palette_image(base_svg_path=SVG_PATH, path=IMAGES_PATH,
#                         palette=DarkPalette):
#    """
#    Create palette image svg and png image on specified path.
#    """
#    # Needed to use QPixmap
#    _ = QApplication([])
#
#    base_palette_svg_path = os.path.join(base_svg_path, 'base_palette.svg')
#    palette_svg_path = os.path.join(path, 'palette.svg')
#    palette_png_path = os.path.join(path, 'palette.png')
#
#    _logger.info("Creating palette image ...")
#    _logger.info("Base SVG: %s" % base_palette_svg_path)
#    _logger.info("To SVG: %s" % palette_svg_path)
#    _logger.info("To PNG: %s" % palette_png_path)
#
#    with open(base_palette_svg_path, 'r') as fh:
#        data = fh.read()
#
#    color_palette = palette.color_palette()
#
#    for color_name, color_value in color_palette.items():
#        data = data.replace('{{ ' + color_name + ' }}', color_value.lower())
#
#    with open(palette_svg_path, 'w+') as fh:
#        fh.write(data)
#
#    convert_svg_to_png(palette_svg_path, palette_png_path, 4000)
#
#    return palette_svg_path, palette_png_path
#

