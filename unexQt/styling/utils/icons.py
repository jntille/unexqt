from os.path import splitext
from PySide2.QtGui import QIcon
from PySide2.QtCore import QSize
from PySide2.QtWidgets import QStyle

def get_icon_mode(mode:QStyle.State):
    if not (mode & QStyle.State_Enabled):
        return QIcon.Mode.Disabled
    if mode & QStyle.State_Selected:
        return QIcon.Mode.Selected
    if mode & QStyle.State_Active or mode & QStyle.State_Sunken or mode & QStyle.State_MouseOver or mode & QStyle.State_HasFocus:
        return QIcon.Mode.Active
    
    return QIcon.Mode.Normal

def get_icon_state(state:QStyle.State):
    if state & QStyle.State_Open:
        return QIcon.State.On
    else:
        return QIcon.State.Off

def get_icon(rc_str:str) -> QIcon:
    icon = QIcon()
    icon_size = QSize(32,32)

    base, ext = splitext(rc_str)
    modes = {
        QIcon.Mode.Normal: '',
        QIcon.Mode.Selected: '_pressed',
        QIcon.Mode.Active: '_focus',
        QIcon.Mode.Disabled: '_disabled'
    }
    template = '{base}{mode}{ext}'
    for icon_mode, mode_suffix in modes.items():
        # find companions rc strings 
        icon.addFile(
            template.format(base=base,mode=mode_suffix,ext=ext),
            size=icon_size,
            mode=icon_mode,
            state=QIcon.State.On
        )
    return icon
 
