from PySide2 import QtWidgets, QtCore, QtGui
stylesheet_A = """
CustomLineEdit {
    color: red;
    background-color: blue;
}
"""
stylesheet_B = """
CustomLineEdit2 {
    background-color: green;
    qproperty-test: #333333;
}
"""
stylesheet_C = """
CustomLineEdit {
    color: red;
    background-color: blue;
}
CustomLineEdit2 {
    background-color: green;
    qproperty-test: #333333;
}
"""
class CustomLineEdit(QtWidgets.QLineEdit):
    def __init__(self, parent):
        super(CustomLineEdit, self).__init__(parent)
        self.setStyleSheet(stylesheet_A)

class CustomLineEdit2(CustomLineEdit):
    def __init__(self, parent=None):
        super(CustomLineEdit2, self).__init__(parent)
        print("a")
        self._test = QtGui.QColor()
    
    def getTest(self):
        return self._test

    def setTest(self, qcolor):
        print("called set with: ", qcolor)
        self._test = qcolor

    #test = QtCore.Property(QtGui.QColor, getTest, setTest)
    test = QtCore.Property(QtGui.QColor, fset=setTest)



if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    app.setStyleSheet(stylesheet_B)
    widget = CustomLineEdit2()
    widget.show()
    print(widget.test)
    app.exec_()

