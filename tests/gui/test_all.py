import sys
from PySide2 import QtWidgets, QtCore, QtGui

from unexQt.widgets.UMainWindow import UMainWindow

from unexQt.widgets.ULabel import ULabelLine, UElidedLabel
from unexQt.widgets.ULineEdit import ULineEdit, UValidateLine, USearchLine, UFindTextPatternLine, UFileBrowserLine
from unexQt.gui.UValidator.UValidator import *
from unexQt.widgets.UButton.UStandardButton import *
from unexQt.widgets.USpinbox import UColorSpinbox, UCropbox
from unexQt.widgets.USlider import USpinboxSlider, UToggle, UExposureSlider, UGammaSlider
from unexQt.widgets.UFileTreeView import UFileTreeWidget
from unexQt.widgets.UTextEdit import UTextEdit

class TestAllWidgets(QtWidgets.QScrollArea):
    def __init__(self):
        super(TestAllWidgets, self).__init__()
        #Scroll Area Properties
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setWidgetResizable(True)

        main_layout = QtWidgets.QFormLayout()
        main_layout.setContentsMargins(0,0,0,0)
        self.widgets = QtWidgets.QWidget()
        self.widgets.setLayout(main_layout)
        self.setWidget(self.widgets)

        self.addWidget(ULabelLine.ULabelLine("some header: "))
        self.addWidget(UElidedLabel.UElidedLabel("somereallylongtextsomereallylongtextsomereallylongtextsomereallylongtext"))

        # ULineEdits
        for line_edit in [ULineEdit.ULineEdit(), UValidateLine.UValidateLine(ASCIIValidator()), USearchLine.USearchLine(), 
            UFindTextPatternLine.UFindTextPatternLine(), UFileBrowserLine.UFileBrowserLine()]:
            self.addWidget(line_edit)

        # UButtons
        standard_buttons = QtWidgets.QHBoxLayout()
        for btn in [UAddButton(), UAppsButton(), UCancelButton(), UDeleteButton(), USaveButton(), UInfoButton(), UWarnButton()]:
            standard_buttons.addWidget(btn)
        self.addWidget(standard_buttons, "UStandardButtons")            

        # Spinbox
        self.addWidget(UColorSpinbox.URGBDoubleDials())
        self.addWidget(UCropbox.UCropbox())


        # Slider
        self.addWidget(USpinboxSlider.UDoubleSpinboxSlider())
        self.addWidget(UGammaSlider.UGammaSlider())
        self.addWidget(UExposureSlider.UExposureSlider())
        
        # FileTreeWidget
        self.addWidget(UFileTreeWidget.UFileTreeWidget())

        # UTextEdit
        text_edit = UTextEdit.UTextEdit()
        text_edit.appendMessage("appended a test line", text_edit.proxy_document)
        self.addWidget(UTextEdit.UTextEdit())


    def addWidget(self, widget, name=None):
        if not name:
            name = type(widget).__name__
        
        self.widgets.layout().addRow(name, widget)
        self.widgets.layout().setVerticalSpacing(10)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    window = UMainWindow.UMainWindow()
    widget = TestAllWidgets()
    window.setCentralWidget(widget)
    
    window.show()
    app.exec_()